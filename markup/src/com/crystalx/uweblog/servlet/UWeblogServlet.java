package com.crystalx.uweblog.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.eclipse.mylyn.wikitext.core.parser.MarkupParser;
import org.eclipse.mylyn.wikitext.core.parser.builder.HtmlDocumentBuilder;
import org.eclipse.mylyn.wikitext.core.parser.markup.IdGenerationStrategy;
import org.eclipse.mylyn.wikitext.core.parser.markup.MarkupLanguage;

import com.crystalx.uweblog.markup.provider.mediawiki.MediaWikiIdGenerationStrategy;
import com.crystalx.uweblog.markup.provider.mediawiki.MediaWikiIdGenerationStrategyProvider;
import com.crystalx.uweblog.markup.wiki.DynamicMarkupLanguage;
import com.crystalx.uweblog.markup.wiki.common.IdGenerationStrategyProvider;
import com.crystalx.uweblog.markup.wiki.common.ImageReferenceResolver;
import com.crystalx.uweblog.markup.wiki.common.InternalPageUrlResolver;

public class UWeblogServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
	    PrintWriter out = response.getWriter();
	    
	    appendWiki(out);
	    
	    out.close();
	}
	
	private void appendWiki(PrintWriter out) throws IOException {
		
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("wiki-markup.txt");

		String markupContent = IOUtils.toString(is, "UTF-8");
		
		InternalPageUrlResolver pageUrlResolver = new InternalPageUrlResolver() {
			@Override
			public String toInternalPageReference(String pageName) {
				return pageName;
			}
		};
		
		ImageReferenceResolver imageReferenceResolver = new ImageReferenceResolver() {
			@Override
			public String resolveImageReference(String imageSrc) {
				return imageSrc;
			}
		};
		
		IdGenerationStrategyProvider idGenerationStrategyProvider = new MediaWikiIdGenerationStrategyProvider();
		
		MarkupLanguage language = new DynamicMarkupLanguage(pageUrlResolver, imageReferenceResolver, idGenerationStrategyProvider);
		StringWriter writer = new StringWriter();

		HtmlDocumentBuilder builder = new HtmlDocumentBuilder(writer);
		builder.setEmitAsDocument(true);

		MarkupParser parser = new MarkupParser();
		parser.setMarkupLanguage(language);
		parser.setBuilder(builder);
		parser.parse(markupContent);

		String htmlContent = writer.toString();
		out.println(htmlContent);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
