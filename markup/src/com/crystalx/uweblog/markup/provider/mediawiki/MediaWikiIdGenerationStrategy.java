package com.crystalx.uweblog.markup.provider.mediawiki;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.mylyn.wikitext.core.parser.markup.IdGenerationStrategy;


public class MediaWikiIdGenerationStrategy extends IdGenerationStrategy {

	private final Map<String, Integer> anchorReferenceCount = new HashMap<String, Integer>();

	@Override
	public String generateId(String headingText) {

		String anchor = headingTextToId(headingText);
		Integer previousRefCount = anchorReferenceCount.put(anchor, 1);
		if (previousRefCount != null) {
			int refCount = previousRefCount + 1;
			anchorReferenceCount.put(anchor, refCount);
			anchor = anchor + '_' + refCount;
		}

		return anchor;
	}

	/**
	 * encode a page name or anchor following MediaWiki encoding behaviour
	 * 
	 * @param headingText
	 *            the heading text, page name or anchor text
	 * @return an encoded id
	 */
	public static String headingTextToId(String headingText) {
		String escaped = headingText.replaceAll("\\s", "_");
		try {
			escaped = URLEncoder.encode(escaped, "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException(e);
		}
		return escaped.replace("%3A", ":").replace('%', '.');
	}
}
