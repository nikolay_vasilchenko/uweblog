package com.crystalx.uweblog.markup.provider.mediawiki;

import org.eclipse.mylyn.wikitext.core.parser.markup.IdGenerationStrategy;

import com.crystalx.uweblog.markup.wiki.common.IdGenerationStrategyProvider;

public class MediaWikiIdGenerationStrategyProvider implements IdGenerationStrategyProvider {

	private static final IdGenerationStrategy strategy = new MediaWikiIdGenerationStrategy();
	
	@Override
	public IdGenerationStrategy getIdGenerationStrategy() {
		return strategy;
	}

}
