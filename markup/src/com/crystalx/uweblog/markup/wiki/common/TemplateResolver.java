
package com.crystalx.uweblog.markup.wiki.common;

/**
 * Dynamically resolve templates by name.
 * 
 */
public abstract class TemplateResolver {

	/**
	 * Resolve a template by its name.
	 * 
	 * @param templateName
	 *            the name of the template
	 * @return the template, or null if the template name is unknown.
	 */
	public abstract Template resolveTemplate(String templateName);
}
