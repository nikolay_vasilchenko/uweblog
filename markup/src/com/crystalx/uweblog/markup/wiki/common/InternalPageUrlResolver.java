package com.crystalx.uweblog.markup.wiki.common;

public interface InternalPageUrlResolver {

	String toInternalPageReference(String pageName);
}
