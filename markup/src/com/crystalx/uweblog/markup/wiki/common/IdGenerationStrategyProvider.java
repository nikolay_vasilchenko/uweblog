package com.crystalx.uweblog.markup.wiki.common;

import org.eclipse.mylyn.wikitext.core.parser.markup.IdGenerationStrategy;

public interface IdGenerationStrategyProvider {
	
	IdGenerationStrategy getIdGenerationStrategy();
}
