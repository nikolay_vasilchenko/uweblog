package com.crystalx.uweblog.markup.wiki.common;

public interface ImageReferenceResolver {

	String resolveImageReference(String imageSrc);
}
