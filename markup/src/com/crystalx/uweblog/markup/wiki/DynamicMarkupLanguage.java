
package com.crystalx.uweblog.markup.wiki;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mylyn.wikitext.core.parser.DocumentBuilder.SpanType;
import org.eclipse.mylyn.wikitext.core.parser.markup.Block;
import org.eclipse.mylyn.wikitext.core.parser.markup.IdGenerationStrategy;
import org.eclipse.mylyn.wikitext.core.parser.markup.MarkupLanguage;
import org.eclipse.mylyn.wikitext.core.parser.markup.phrase.HtmlCommentPhraseModifier;
import org.eclipse.mylyn.wikitext.core.parser.markup.phrase.LimitedHtmlEndTagPhraseModifier;
import org.eclipse.mylyn.wikitext.core.parser.markup.phrase.LimitedHtmlStartTagPhraseModifier;
import org.eclipse.mylyn.wikitext.core.parser.markup.token.EntityReferenceReplacementToken;
import org.eclipse.mylyn.wikitext.core.parser.markup.token.ImpliedHyperlinkReplacementToken;
import org.eclipse.mylyn.wikitext.core.parser.markup.token.PatternLiteralReplacementToken;

import com.crystalx.uweblog.markup.wiki.common.IdGenerationStrategyProvider;
import com.crystalx.uweblog.markup.wiki.common.ImageReferenceResolver;
import com.crystalx.uweblog.markup.wiki.common.InternalPageUrlResolver;
import com.crystalx.uweblog.markup.wiki.common.Template;
import com.crystalx.uweblog.markup.wiki.common.TemplateResolver;
import com.crystalx.uweblog.markup.wiki.core.TemplateBasedMarkupLanguage;
import com.crystalx.uweblog.markup.wiki.core.block.BehaviorSwitchBlock;
import com.crystalx.uweblog.markup.wiki.core.block.CommentBlock;
import com.crystalx.uweblog.markup.wiki.core.block.EscapeBlock;
import com.crystalx.uweblog.markup.wiki.core.block.HeadingBlock;
import com.crystalx.uweblog.markup.wiki.core.block.ListBlock;
import com.crystalx.uweblog.markup.wiki.core.block.ParagraphBlock;
import com.crystalx.uweblog.markup.wiki.core.block.PreformattedBlock;
import com.crystalx.uweblog.markup.wiki.core.block.SourceBlock;
import com.crystalx.uweblog.markup.wiki.core.block.TableBlock;
import com.crystalx.uweblog.markup.wiki.core.block.TableOfContentsBlock;
import com.crystalx.uweblog.markup.wiki.core.phrase.EscapePhraseModifier;
import com.crystalx.uweblog.markup.wiki.core.phrase.SimplePhraseModifier;
import com.crystalx.uweblog.markup.wiki.core.template.BuiltInTemplateResolver;
import com.crystalx.uweblog.markup.wiki.core.token.DefaultEntityReferenceReplacementToken;
import com.crystalx.uweblog.markup.wiki.core.token.HyperlinkExternalReplacementToken;
import com.crystalx.uweblog.markup.wiki.core.token.HyperlinkInternalReplacementToken;
import com.crystalx.uweblog.markup.wiki.core.token.ImageReplacementToken;
import com.crystalx.uweblog.markup.wiki.core.token.LineBreakToken;

public class DynamicMarkupLanguage extends TemplateBasedMarkupLanguage {

	private static final String[] allowedHtmlTags = new String[] {
			// HANDLED BY LineBreakToken "<br>",
			// HANDLED BY LineBreakToken "<br/>",
			"b", "big", "blockquote", "caption", "center", "cite", "code", "dd", "del", "div", "dl", "dt", "em",
			"font", "h1", "h2", "h3", "h4", "h5", "h6", "hr", "i", "ins", "li", "ol", "p", "pre", "rb", "rp",
			"rt", "ruby", "s", "small", "span", "strike", "strong", "sub", "sup", "table", "td", "th", "tr",
			"tt", "u", "ul", "var" 
	};

	
	
	private InternalPageUrlResolver internalPageUrlResolver;
	private ImageReferenceResolver imageReferenceResolver;
	private IdGenerationStrategyProvider idGenerationStrategyProvider;
	
	private List<Template> templates = new ArrayList<Template>();
	private List<TemplateResolver> templateProviders = new ArrayList<TemplateResolver>();
	private String templateExcludes;
	
	
	/**
	 * For serializtion and clone only!
	 */
	public DynamicMarkupLanguage() {

	}

	public DynamicMarkupLanguage(
			InternalPageUrlResolver internalPageUrlResolver,
			ImageReferenceResolver imageReferenceResolver,
			IdGenerationStrategyProvider idGenerationStrategyProvider) {
		
		this.internalPageUrlResolver = internalPageUrlResolver;
		this.imageReferenceResolver = imageReferenceResolver;
		this.idGenerationStrategyProvider = idGenerationStrategyProvider;
		
		setName("DynamicMarkupLanguage");

		templateProviders.add(new BuiltInTemplateResolver());
	}

	@Override
	public IdGenerationStrategy getIdGenerationStrategy() {
		return idGenerationStrategyProvider.getIdGenerationStrategy();
	}

	@Override
	protected void addStandardBlocks(List<Block> blocks, List<Block> paragraphBreakingBlocks) {
		// IMPORTANT NOTE: Most items below have order dependencies.  DO NOT REORDER ITEMS BELOW!!

		blocks.add(new HeadingBlock());
		blocks.add(new ListBlock());
		blocks.add(new TableBlock());

		if (hasPreformattedBlock()) {
			// preformatted blocks are lines that start with a single space, and thus are non-optimal for
			// repository usage.
			blocks.add(new PreformattedBlock());
		}
		blocks.add(new SourceBlock());

		blocks.add(new TableOfContentsBlock(this));
		blocks.add(new EscapeBlock());
		blocks.add(new CommentBlock());
		blocks.add(new BehaviorSwitchBlock());

		for (Block block : blocks) {
			if (block instanceof ParagraphBlock || block instanceof CommentBlock) {
				continue;
			}
			paragraphBreakingBlocks.add(block);
		}

	}

	private boolean hasPreformattedBlock() {
		return configuration == null ? true : !configuration.isOptimizeForRepositoryUsage();
	}

	@Override
	protected void addStandardPhraseModifiers(PatternBasedSyntax phraseModifierSyntax) {
		
		phraseModifierSyntax.add(new EscapePhraseModifier());
		phraseModifierSyntax.beginGroup("(?:(?<=[\\s\\.,\\\"'?!;:\\)\\(\\{\\}\\[\\]=>])|^)(?:", 0);
		phraseModifierSyntax.add(new SimplePhraseModifier("'''''", new SpanType[] { SpanType.BOLD, SpanType.ITALIC }, true));
		phraseModifierSyntax.add(new SimplePhraseModifier("'''", SpanType.BOLD, true));
		phraseModifierSyntax.add(new SimplePhraseModifier("''", SpanType.ITALIC, true));
		phraseModifierSyntax.endGroup(")", 0);

		boolean escapingHtml = configuration == null ? false : configuration.isEscapingHtmlAndXml();

		if (!escapingHtml) {
			phraseModifierSyntax.add(new LimitedHtmlEndTagPhraseModifier(allowedHtmlTags));
			phraseModifierSyntax.add(new LimitedHtmlStartTagPhraseModifier(allowedHtmlTags));
			phraseModifierSyntax.add(new HtmlCommentPhraseModifier());
		}
	}

	@Override
	protected void addStandardTokens(PatternBasedSyntax tokenSyntax) {
		tokenSyntax.add(new LineBreakToken());
		tokenSyntax.add(new EntityReferenceReplacementToken("(tm)", "#8482"));
		tokenSyntax.add(new EntityReferenceReplacementToken("(TM)", "#8482"));
		tokenSyntax.add(new EntityReferenceReplacementToken("(c)", "#169"));
		tokenSyntax.add(new EntityReferenceReplacementToken("(C)", "#169"));
		tokenSyntax.add(new EntityReferenceReplacementToken("(r)", "#174"));
		tokenSyntax.add(new EntityReferenceReplacementToken("(R)", "#174"));
		tokenSyntax.add(new ImageReplacementToken(imageReferenceResolver));
		tokenSyntax.add(new HyperlinkInternalReplacementToken(internalPageUrlResolver));
		tokenSyntax.add(new HyperlinkExternalReplacementToken());
		tokenSyntax.add(new ImpliedHyperlinkReplacementToken());
		tokenSyntax.add(new PatternLiteralReplacementToken("(?:(?<=^|\\w\\s)(----)(?=$|\\s\\w))", "<hr/>")); // horizontal rule
		tokenSyntax.add(new DefaultEntityReferenceReplacementToken());
	}

	@Override
	protected Block createParagraphBlock() {
		ParagraphBlock paragraphBlock = new ParagraphBlock(hasPreformattedBlock());
		if (configuration != null && configuration.isNewlinesMustCauseLineBreak()) {
			paragraphBlock.setNewlinesCauseLineBreak(true);
		}
		return paragraphBlock;
	}

	/**
	 * @since 1.3
	 */
	@Override
	public List<Template> getTemplates() {
		return templates;
	}

	/**
	 * @since 1.3
	 */
	public void setTemplates(List<Template> templates) {
		if (templates == null) {
			throw new IllegalArgumentException();
		}
		this.templates = templates;
	}

	/**
	 * @since 1.3
	 */
	@Override
	public List<TemplateResolver> getTemplateProviders() {
		return templateProviders;
	}

	/**
	 * @since 1.3
	 */
	public void setTemplateProviders(List<TemplateResolver> templateProviders) {
		if (templateProviders == null) {
			throw new IllegalArgumentException();
		}
		this.templateProviders = templateProviders;
	}

	@Override
	public MarkupLanguage clone() {
		DynamicMarkupLanguage copy = (DynamicMarkupLanguage) super.clone();
		copy.templates = new ArrayList<Template>(templates);
		copy.templateProviders = new ArrayList<TemplateResolver>(templateProviders);
		copy.templateExcludes = templateExcludes;
		copy.internalPageUrlResolver = internalPageUrlResolver;
		copy.imageReferenceResolver = imageReferenceResolver;
		copy.idGenerationStrategyProvider = idGenerationStrategyProvider;
		return copy;
	}

	/**
	 * Indicate template names to exclude.
	 *
	 * @param templateExcludes
	 *            a comma-delimited list of names, may include '*' wildcards
	 * @since 1.3
	 */
	public void setTemplateExcludes(String templateExcludes) {
		this.templateExcludes = templateExcludes;
	}

	/**
	 * Indicate template names to exclude.
	 *
	 * @return a comma-delimited list of names, may include '*' wildcards, or null if none are to be excluded
	 * @since 1.3
	 */
	@Override
	public String getTemplateExcludes() {
		return templateExcludes;
	}

}
