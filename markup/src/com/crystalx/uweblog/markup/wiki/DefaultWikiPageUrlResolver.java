package com.crystalx.uweblog.markup.wiki;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.crystalx.uweblog.markup.wiki.common.InternalPageUrlResolver;

public class DefaultWikiPageUrlResolver implements InternalPageUrlResolver {

	private static final String CATEGORY_PREFIX = ":";
	private static final Pattern STANDARD_EXTERNAL_LINK_FORMAT = Pattern.compile(".*?/([^/]+)/(\\{\\d+\\})");
	private static final Pattern QUALIFIED_INTERNAL_LINK = Pattern.compile("([^/]+)/(.+)");
	
	protected String internalLinkPattern = "{0}";

	public DefaultWikiPageUrlResolver() {
		
	}
	
	public DefaultWikiPageUrlResolver(String internalLinkPattern) {
		this.internalLinkPattern = internalLinkPattern;
	}
	
	public String toInternalPageReference(String pageName) {

		String pageId;
		int anchorIndex = pageName.indexOf("#");
		if (anchorIndex < 0) {
			pageId = pageName;
		} else {
			String encodedAnchor = headingTextToId(pageName.substring(anchorIndex + 1));
			pageId = pageName.substring(0, anchorIndex) + "#" + encodedAnchor;
		}

		if (pageId.startsWith(CATEGORY_PREFIX) && pageId.length() > CATEGORY_PREFIX.length()) {
			return pageId.substring(CATEGORY_PREFIX.length());
		} else if (pageId.startsWith("#")) {
			// internal anchor
			return pageId;
		}
		// XXX
		if (internalLinkPattern.contains("index.php?")) {
			try {
				pageId = URLEncoder.encode(pageId, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new IllegalStateException(MessageFormat.format("Cannot compute url: {0}", pageId), e);
			}
		}
		if (QUALIFIED_INTERNAL_LINK.matcher(pageId).matches()) {
			Matcher matcher = STANDARD_EXTERNAL_LINK_FORMAT.matcher(internalLinkPattern);
			if (matcher.matches()) {
				String prefix = matcher.group(1);
				if (pageId.startsWith(prefix + '/')) {
					return internalLinkPattern.substring(0, matcher.start(1)) + pageId;
				} else {
					return internalLinkPattern.substring(0, matcher.start(2)) + pageId;
				}
			}
		}
		return MessageFormat.format(internalLinkPattern, pageId);
	}

	private String headingTextToId(String headingText) {

		String escaped = headingText.replaceAll("\\s", "_");
		try {
			escaped = URLEncoder.encode(escaped, "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException(e);
		}
		return escaped.replace("%3A", ":").replace('%', '.');
	}

}
