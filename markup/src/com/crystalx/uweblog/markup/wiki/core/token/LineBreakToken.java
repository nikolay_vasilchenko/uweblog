package com.crystalx.uweblog.markup.wiki.core.token;

import org.eclipse.mylyn.wikitext.core.parser.markup.PatternBasedElement;
import org.eclipse.mylyn.wikitext.core.parser.markup.PatternBasedElementProcessor;

public class LineBreakToken extends PatternBasedElement {

	@Override
	protected String getPattern(int groupOffset) {
		return "(<br>|<br/>)";
	}

	@Override
	protected int getPatternGroupCount() {
		return 1;
	}

	@Override
	protected PatternBasedElementProcessor newProcessor() {
		return new LineBreakProcessor();
	}

	private static class LineBreakProcessor extends PatternBasedElementProcessor {
		@Override
		public void emit() {
			builder.lineBreak();
		}
	}
}
