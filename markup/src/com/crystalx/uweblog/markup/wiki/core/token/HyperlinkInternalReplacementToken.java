package com.crystalx.uweblog.markup.wiki.core.token;

import org.eclipse.mylyn.wikitext.core.parser.Attributes;
import org.eclipse.mylyn.wikitext.core.parser.LinkAttributes;
import org.eclipse.mylyn.wikitext.core.parser.markup.PatternBasedElement;
import org.eclipse.mylyn.wikitext.core.parser.markup.PatternBasedElementProcessor;

import com.crystalx.uweblog.markup.wiki.common.InternalPageUrlResolver;

/**
 * match [[internal links]]
 * 
 */
public class HyperlinkInternalReplacementToken extends PatternBasedElement {

	private final InternalPageUrlResolver internalHrefResolver;
	
	public HyperlinkInternalReplacementToken(InternalPageUrlResolver internalHrefResolver) {
		this.internalHrefResolver = internalHrefResolver;
	}
	
	@Override
	protected String getPattern(int groupOffset) {
		return "(?:\\[\\[([^\\]\\|]+?)\\s*(?:\\|\\s*([^\\]]*))?\\]\\])";
	}

	@Override
	protected int getPatternGroupCount() {
		return 2;
	}

	@Override
	protected PatternBasedElementProcessor newProcessor() {
		return new HyperlinkReplacementTokenProcessor(internalHrefResolver);
	}
	
	private static class HyperlinkReplacementTokenProcessor extends PatternBasedElementProcessor {
		
		private final InternalPageUrlResolver internalHrefResolver;
		
		public HyperlinkReplacementTokenProcessor(InternalPageUrlResolver internalHrefResolver) {
			this.internalHrefResolver = internalHrefResolver;
		}
		
		@Override
		public void emit() {
			String pageName = group(1);
			String altText = group(2);
			String href = internalHrefResolver.toInternalPageReference(pageName);

			if (pageName.startsWith(":")) {
				pageName = pageName.substring(1);
			}
			if (altText == null || altText.trim().length() == 0) {
				altText = pageName;
				if (altText.startsWith("#")) {
					altText = altText.substring(1);
				}
			}
			if (pageName.startsWith("#")) {
				builder.link(href, altText);
			} else {
				Attributes attributes = new LinkAttributes();
				attributes.setTitle(pageName);
				builder.link(attributes, href, altText);
			}
		}
	}


}
