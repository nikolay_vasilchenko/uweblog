package com.crystalx.uweblog.markup.wiki.core.template;

import java.util.HashMap;
import java.util.Map;

import com.crystalx.uweblog.markup.wiki.common.Template;
import com.crystalx.uweblog.markup.wiki.common.TemplateResolver;

public class BuiltInTemplateResolver extends TemplateResolver {

	private static Map<String, Template> builtInTemplates = new HashMap<String, Template>();
	static {
		registerTemplate(new Template("mdash", "&nbsp;&mdash; "));
		registerTemplate(new Template("mdash", "&nbsp;&mdash; "));
		registerTemplate(new Template("ndash", "&nbsp;&ndash; "));
		registerTemplate(new Template("emdash", "&nbsp;&mdash; "));
		registerTemplate(new Template("endash", "&nbsp;&ndash; "));
		registerTemplate(new DateTimeTemplate("CURRENTYEAR", "yyyy"));
		registerTemplate(new DateTimeTemplate("CURRENTMONTH", "MM"));
		registerTemplate(new DateTimeTemplate("CURRENTMONTHNAME", "MMMMMMMM"));
		registerTemplate(new DateTimeTemplate("CURRENTMONTHNAMEGEN", "MMMMMMMM"));
		registerTemplate(new DateTimeTemplate("CURRENTMONTHABBREV", "MMM"));
		registerTemplate(new DateTimeTemplate("CURRENTDAY", "dd"));
		registerTemplate(new DateTimeTemplate("CURRENTDAY2", "dd"));
		registerTemplate(new DateTimeTemplate("CURRENTDOW", "F"));
		registerTemplate(new DateTimeTemplate("CURRENTDAYNAME", "EEEEEEEE"));
		registerTemplate(new DateTimeTemplate("CURRENTTIME", "HH:mm"));
		registerTemplate(new DateTimeTemplate("CURRENTHOUR", "HH"));
		registerTemplate(new DateTimeTemplate("CURRENTWEEK", "ww"));
		registerTemplate(new DateTimeTemplate("CURRENTTIMESTAMP", "yyyyMMddHHmmss"));
	}

	@Override
	public Template resolveTemplate(String templateName) {
		return builtInTemplates.get(templateName);
	}

	private static void registerTemplate(Template template) {
		builtInTemplates.put(template.getName(), template);
	}

}
