package com.crystalx.uweblog.markup.wiki.core.phrase;

import org.eclipse.mylyn.wikitext.core.parser.markup.PatternBasedElement;
import org.eclipse.mylyn.wikitext.core.parser.markup.PatternBasedElementProcessor;
import org.eclipse.mylyn.wikitext.core.parser.markup.phrase.LiteralPhraseModifierProcessor;

public class EscapePhraseModifier extends PatternBasedElement {

	@Override
	public String getPattern(int groupOffset) {
		return "<nowiki>" + "((?:(?!</nowiki>).)*)</nowiki>";
	}

	@Override
	protected int getPatternGroupCount() {
		return 1;
	}

	@Override
	protected PatternBasedElementProcessor newProcessor() {
		return new LiteralPhraseModifierProcessor(true);
	}

}
