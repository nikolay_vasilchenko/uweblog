package com.crystalx.uweblog.markup.wiki.core.block;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.core.parser.Attributes;
import org.eclipse.mylyn.wikitext.core.parser.markup.Block;

public class HeadingBlock extends Block {

	private static final Pattern pattern = Pattern.compile("\\s*(\\={1,6})\\s*(.+?)\\s*\\1\\s*");

	private int blockLineCount = 0;
	private Matcher matcher;

	@Override
	public boolean canStart(String line, int lineOffset) {
		blockLineCount = 0;
		if (lineOffset == 0) {
			matcher = pattern.matcher(line);
			return matcher.matches();
		} else {
			matcher = null;
			return false;
		}
	}

	@Override
	public int processLineContent(String line, int offset) {
		if (blockLineCount > 0) {
			throw new IllegalStateException();
		}
		++blockLineCount;

		int level = matcher.group(1).length();

		String text = matcher.group(2);

		final Attributes attributes = new Attributes();
		if (attributes.getId() == null) {
			// bug 374019: strip HTML tags from text used to generate ID
			String textForId = text.replaceAll("</?[a-zA-Z0-9]+.*?>", "");

			attributes.setId(state.getIdGenerator().newId("h" + level, textForId));
		}
		builder.beginHeading(level, attributes);

		offset = matcher.start(2);
		line = text;
		getMarkupLanguage().emitMarkupLine(getParser(), state, offset, line, 0);

		builder.endHeading();

		setClosed(true);
		return -1;
	}

}
