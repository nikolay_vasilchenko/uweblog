
package com.crystalx.uweblog.markup.wiki.core.block;

import org.eclipse.mylyn.wikitext.core.parser.Attributes;
import org.eclipse.mylyn.wikitext.core.parser.DocumentBuilder.BlockType;
import org.eclipse.mylyn.wikitext.core.parser.markup.block.AbstractHtmlBlock;

/**
 * A block that implements the syntax highlighting extension
 * 
 */
public class SourceBlock extends AbstractHtmlBlock {

	public SourceBlock() {
		super("source");
	}

	private String lang;

	@Override
	protected void handleAttribute(String attrName, String attrValue) {
		if (attrName.equalsIgnoreCase("lang")) {
			lang = attrValue;
		}
	}

	@Override
	protected void beginBlock() {
		final Attributes attributes = new Attributes();
		if (lang != null) {
			attributes.setCssClass("source-" + lang);
		}
		builder.beginBlock(BlockType.PREFORMATTED, attributes);
	}

	@Override
	protected void endBlock() {
		builder.endBlock();
	}

	@Override
	protected void handleBlockContent(String content) {
		if (content.length() > 0) {
			builder.characters(content);
		} else if (blockLineCount == 1) {
			return;
		}
		builder.characters("\n");
	}

}
