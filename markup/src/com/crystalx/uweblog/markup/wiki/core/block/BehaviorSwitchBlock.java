package com.crystalx.uweblog.markup.wiki.core.block;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.core.parser.markup.Block;

/**
 * a block for MediaWiki <a href="http://www.mediawiki.org/wiki/Help:Magic_words#Behavior_switches">behavior
 * switches</a> (processed as empty string).
 *
 * @See also {@link TableOfContentsBlock}
 */
public class BehaviorSwitchBlock extends Block {

	private static final Pattern PATTERN = Pattern.compile(
			"__(FORCE|NO)?(EDIT|NEW)?(CC|CONTENT|DISAMBIG|END|GALLERY|HIDDENCAT|INDEX|SECTION|START|STATICREDIRECT|TC|TITLE|TOC)(CONVERT|LINK)?__");

	private Matcher matcher;

	@Override
	public boolean canStart(String line, int lineOffset) {
		matcher = PATTERN.matcher(line);
		if (lineOffset > 0) {
			matcher.region(lineOffset, line.length());
		}
		if (matcher.matches()) {
			return true;
		}
		matcher = null;
		return false;
	}

	@Override
	protected int processLineContent(String line, int offset) {
		setClosed(true);
		return matcher.end();
	}
}
