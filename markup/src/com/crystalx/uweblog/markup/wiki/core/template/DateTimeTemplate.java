package com.crystalx.uweblog.markup.wiki.core.template;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.crystalx.uweblog.markup.wiki.common.Template;

public class DateTimeTemplate extends Template {

	public DateTimeTemplate() {
	}

	/**
	 * @param name
	 * @param templateMarkup
	 *            the format to use with {@link SimpleDateFormat}.
	 */
	public DateTimeTemplate(String name, String templateMarkup) {
		super(name, templateMarkup);
	}

	@Override
	public String getTemplateContent() {
		String markup = getTemplateMarkup();
		if (markup != null) {
			SimpleDateFormat format = new SimpleDateFormat(markup);
			markup = format.format(new Date());
		}
		return markup;
	}
}
