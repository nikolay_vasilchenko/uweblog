package com.crystalx.uweblog.markup.wiki.core;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.core.parser.MarkupParser;
import org.eclipse.mylyn.wikitext.core.parser.markup.AbstractMarkupLanguage;

import com.crystalx.uweblog.markup.wiki.common.Template;
import com.crystalx.uweblog.markup.wiki.common.TemplateResolver;


public abstract class TemplateBasedMarkupLanguage extends AbstractMarkupLanguage {

	private static final Pattern templatePattern = Pattern.compile("(?:^|(?<!\\{))(\\{\\{(#?[a-zA-Z0-9_ :/()\\.\\-]+)\\s*(\\|[^\\}]*)?\\}\\})");
	private static final Pattern templateParameterPattern = Pattern.compile("\\{\\{\\{([a-zA-Z0-9]+)(?:\\|([^\\}]*))?\\}\\}\\}");
	private static final Pattern parameterSpec = Pattern.compile("\\|\\s*([^\\|=]+)(?:\\s*=\\s*(([^|]*)))?");
	private static final Pattern includeOnlyPattern = Pattern.compile(".*?<includeonly>(.*?)</includeonly>.*", Pattern.DOTALL);
	private static final Pattern noIncludePattern = Pattern.compile("<noinclude>(.*?)</noinclude>", Pattern.DOTALL);
	
	public abstract List<Template> getTemplates();
	public abstract List<TemplateResolver> getTemplateProviders();
	public abstract String getTemplateExcludes();
	
	@Override
	public void processContent(MarkupParser parser, String markupContent, boolean asDocument) {
		if (isEnableMacros()) {
			markupContent = preprocessContent(markupContent);
		}
		super.processContent(parser, markupContent, asDocument);
	}

	private String preprocessContent(String markupContent) {
		
		Map<String, Template> templateByName = new HashMap<String, Template>();
		List<Pattern> excludePatterns = new ArrayList<Pattern>();
		

		for (Template template : getTemplates()) {
			templateByName.put(template.getName(), normalize(template));
		}
		String templateExcludes = getTemplateExcludes();
		if (templateExcludes != null) {
			String[] split = templateExcludes.split("\\s*,\\s*");
			for (String exclude : split) {
				String pattern = exclude.replaceAll("([^a-zA-Z:\\*])", "\\\\$1").replaceAll("\\*", ".*?");
				excludePatterns.add(Pattern.compile(pattern, Pattern.CASE_INSENSITIVE));
			}
		}
		
		return processTemplates(markupContent, templateByName, excludePatterns);
	}
	
	private String processTemplates(String markupContent, Map<String, Template> templateByName, List<Pattern> excludePatterns) {
		return processTemplates(markupContent, Collections.<String> emptySet(), templateByName, excludePatterns);
	}

	private String processTemplates(String markupContent, Set<String> usedTemplates, Map<String, Template> templateByName, List<Pattern> excludePatterns) {
		StringBuilder processedMarkup = new StringBuilder();

		int lastIndex = 0;
		Matcher matcher = templatePattern.matcher(markupContent);
		while (matcher.find()) {
			int start = matcher.start();
			if (lastIndex < start) {
				processedMarkup.append(markupContent.substring(lastIndex, start));
			}
			String templateName = matcher.group(2);
			Template template = resolveTemplate(templateName, templateByName, excludePatterns);
			if (template != null) {
				String replacementText;
				if (usedTemplates.contains(templateName)) {
					StringBuilder sb = new StringBuilder();
					sb.append("<span class=\"error\">");
					sb.append(MessageFormat.format("Template loop detected:{0}", template.getName()));
					sb.append("</span>");
					replacementText = sb.toString();
				} else {
					String parameters = matcher.group(3);
					replacementText = processTemplate(template, parameters);
					//The replacementText might contain other templates. Add the current template to the set of used template and call recursively this function again:
					Set<String> templates = new HashSet<String>(usedTemplates);
					templates.add(templateName);
					replacementText = processTemplates(replacementText, templates, templateByName, excludePatterns);
				}
				replacementText = processTemplates(replacementText, templateByName, excludePatterns);
				processedMarkup.append(replacementText);
			}
			lastIndex = matcher.end();
		}
		if (lastIndex == 0) {
			return markupContent;
		}
		if (lastIndex < markupContent.length()) {
			processedMarkup.append(markupContent.substring(lastIndex));
		}
		return processedMarkup.toString();
	}

	private String processTemplate(Template template, String parametersText) {
		if (template.getTemplateMarkup() == null) {
			return "";
		}
		String macro = template.getTemplateContent();

		List<Parameter> parameters = processParameters(parametersText);

		StringBuilder processedMarkup = new StringBuilder();
		int lastIndex = 0;
		Matcher matcher = templateParameterPattern.matcher(macro);
		while (matcher.find()) {
			int start = matcher.start();
			if (lastIndex < start) {
				processedMarkup.append(macro.substring(lastIndex, start));
			}
			String parameterName = matcher.group(1);
			String parameterValue = matcher.group(2);
			try {
				int parameterIndex = Integer.parseInt(parameterName);
				if (parameterIndex <= parameters.size() && parameterIndex > 0) {
					parameterValue = parameters.get(parameterIndex - 1).value;
				}
			} catch (NumberFormatException e) {
				for (Parameter param : parameters) {
					if (parameterName.equalsIgnoreCase(param.name)) {
						parameterValue = param.value;
						break;
					}
				}
			}
			if (parameterValue != null) {
				processedMarkup.append(parameterValue);
			}

			lastIndex = matcher.end();
		}
		if (lastIndex == 0) {
			return macro;
		}
		if (lastIndex < macro.length()) {
			processedMarkup.append(macro.substring(lastIndex));
		}
		return processedMarkup.toString();
	}

	private static List<Parameter> processParameters(String parametersText) {
		List<Parameter> parameters = new ArrayList<Parameter>();
		if (parametersText != null && parametersText.length() > 0) {
			Matcher matcher = parameterSpec.matcher(parametersText);
			while (matcher.find()) {
				String nameOrValue = matcher.group(1);
				String value = matcher.group(2);
				Parameter parameter = new Parameter();
				if (value != null) {
					parameter.name = nameOrValue;
					parameter.value = value;
				} else {
					parameter.value = nameOrValue;
				}
				parameters.add(parameter);
			}
		}
		return parameters;
	}

	private Template resolveTemplate(String templateName, Map<String, Template> templateByName, List<Pattern> excludePatterns) {
		if (!excludePatterns.isEmpty()) {
			for (Pattern p : excludePatterns) {
				if (p.matcher(templateName).matches()) {
					return null;
				}
			}
		}
		Template template = templateByName.get(templateName);
		if (template == null) {
			for (TemplateResolver resolver : getTemplateProviders()) {
				template = resolver.resolveTemplate(templateName);
				if (template != null) {
					template = normalize(template);
					break;
				}
			}
			if (template == null) {
				template = new Template();
				template.setName(templateName);
				template.setTemplateMarkup("");
			}
			templateByName.put(template.getName(), template);
		}
		return template;
	}

	private static Template normalize(Template template) {
		Template normalizedTemplate = new Template();
		normalizedTemplate.setName(template.getName());
		normalizedTemplate.setTemplateMarkup(normalizeTemplateMarkup(template.getTemplateContent()));

		return normalizedTemplate;
	}

	private static String normalizeTemplateMarkup(String templateMarkup) {
		Matcher matcher = includeOnlyPattern.matcher(templateMarkup);
		if (matcher.matches()) {
			return matcher.group(1);
		}
		matcher = noIncludePattern.matcher(templateMarkup);
		return matcher.replaceAll("");
	}

	private static class Parameter {
		String name;

		String value;
	}

}
