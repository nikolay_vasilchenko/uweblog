package com.crystalx.uweblog.markup.wiki.core.block;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.core.parser.markup.MarkupLanguage;
import org.eclipse.mylyn.wikitext.core.parser.outline.OutlineItem;
import org.eclipse.mylyn.wikitext.core.parser.outline.OutlineParser;

public class TableOfContentsBlock extends AbstractTableOfContentsBlock {

	static final Pattern startPattern = Pattern.compile("\\s*__TOC__\\s*(.*?)");

	private final MarkupLanguage currentMarkupLanguage;
	private int blockLineNumber = 0;
	private Matcher matcher;
	
	public TableOfContentsBlock(MarkupLanguage currentMarkupLanguage) {
		this.currentMarkupLanguage = currentMarkupLanguage;
	}

	@Override
	public int processLineContent(String line, int offset) {
		if (blockLineNumber++ > 0) {
			setClosed(true);
			return 0;
		}

		if (!getMarkupLanguage().isFilterGenerativeContents()) {
			OutlineParser outlineParser = new OutlineParser(currentMarkupLanguage);
			OutlineItem rootItem = outlineParser.parse(state.getMarkupContent());

			emitToc(rootItem);
		}
		int start = matcher.start(1);
		if (start > 0) {
			setClosed(true);
		}
		return start;
	}

	@Override
	public boolean canStart(String line, int lineOffset) {
		if (lineOffset == 0 && !getMarkupLanguage().isFilterGenerativeContents()) {
			matcher = startPattern.matcher(line);
			blockLineNumber = 0;
			return matcher.matches();
		} else {
			matcher = null;
			return false;
		}
	}

}
