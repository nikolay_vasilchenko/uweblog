package com.crystalx.uweblog.markup.wiki.core.token;

import org.eclipse.mylyn.wikitext.core.parser.markup.PatternBasedElement;
import org.eclipse.mylyn.wikitext.core.parser.markup.PatternBasedElementProcessor;

import com.crystalx.uweblog.markup.wiki.common.ImageReferenceResolver;

public class HyperlinkReplacementToken extends PatternBasedElement {

	private final ImageReferenceResolver imageReferenceResolver;
	
	public HyperlinkReplacementToken(ImageReferenceResolver imageReferenceResolver) {
		this.imageReferenceResolver = imageReferenceResolver;
	}
	
	@Override
	protected String getPattern(int groupOffset) {
		return "(?:(\"|\\!)([^\"]+)\\" + (1 + groupOffset) + ":([^\\s]+))";
	}

	@Override
	protected int getPatternGroupCount() {
		return 3;
	}

	@Override
	protected PatternBasedElementProcessor newProcessor() {
		return new HyperlinkReplacementTokenProcessor(imageReferenceResolver);
	}

	private static class HyperlinkReplacementTokenProcessor extends PatternBasedElementProcessor {
		
		private final ImageReferenceResolver imageReferenceResolver;
		
		HyperlinkReplacementTokenProcessor(ImageReferenceResolver imageReferenceResolver) {
			this.imageReferenceResolver = imageReferenceResolver;
		}
		
		@Override
		public void emit() {
			String hyperlinkBoundaryText = group(1);
			String hyperlinkSrc = group(2);
			String href = group(3);

			if (hyperlinkBoundaryText.equals("\"")) {
				builder.link(href, hyperlinkSrc);
			} else {
				builder.imageLink(href, imageReferenceResolver.resolveImageReference(hyperlinkSrc));
			}
		}

	}

}
