package com.crystalx.repository.file;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.FileSystems;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.crystalx.repository.common.ConnectionMode;
import com.crystalx.repository.common.Document;
import com.crystalx.repository.common.RepositoryController;
import com.crystalx.repository.common.SourceManageService;
import com.crystalx.repository.common.security.Authorizable;
import com.crystalx.repository.common.security.SecurityContext;
import com.crystalx.repository.common.security.SecurityPrivileges;
import com.crystalx.repository.exception.NoSuchObjectException;
import com.crystalx.repository.exception.RepositoryException;
import com.crystalx.repository.exception.SecurityException;

public class FileBasedRepositoryController implements RepositoryController, SecurityContext {

	private static final Logger logger = Logger.getLogger(FileBasedRepositoryController.class);
	
	private final String LOCK_FILE_NAME = ".rlock";
	private final String PSEPARATOR = FileSystems.getDefault().getSeparator();
	private final String MODULE_SUFFIX = "D";
	private final String FILE_SUFFIX = "F";
	
	private String location;
	private ConnectionMode mode;
	private boolean locked = false;
	
	private final SourceManageService sourceManageService;
	
	protected FileBasedRepositoryController(SourceManageService sourceManageService) {
		this.sourceManageService = sourceManageService;
	}
	
	public void attachRepository(String location, ConnectionMode mode) throws RepositoryException {
	
		while(location.endsWith(PSEPARATOR)) {
			location = location.substring(0,  location.length() - 1);
		}
		
		this.location = location;
		this.mode = mode;
		
		File locker = new File(location + PSEPARATOR + LOCK_FILE_NAME);
		
		boolean repositoryLock = true;
		try {
			repositoryLock = locker.exists() && FileUtilsEx.checkFileLock(locker);
		} catch (IOException e) {
			throwException("Failed to check repository file lock: ", e);

		}
		
		if( mode == ConnectionMode.MANAGE) {
			
			if(repositoryLock) {
				throwException("Failed to attach repository. Another process already locked this instance");
			} else {
				attemptToLockReposytory(locker);	
			}
		} else {
			locked = false;
		}
		
		
	}
	
	public void releaseRepository() throws RepositoryException {
		
		if(mode != ConnectionMode.MANAGE) {
			return;
		}
		
		if(!locked) {
			return;
		}
		
		File locker = new File(location + PSEPARATOR + LOCK_FILE_NAME);
		try {
			if(locker.exists()) {
				FileUtils.forceDelete(locker);
			}
		} catch (IOException e) {
			throwException("Failed to unlock repository: ", e);
		} finally {
			locked = false;
		}

	}
	
	@Override
	public void close() throws IOException {
		try {
			releaseRepository();
		} catch (RepositoryException e) {
			throw new IOException(e);
		}		
	}
	


	public String getLocation() {
		return location;
	}
	
	@Override
	public ConnectionMode getMode() {
		return mode;
	}
	
	@Override
	public void createModule(String moduleName) throws RepositoryException {
		moduleName = encodePath(moduleName);
		
		String path = location;
		
		String parts[] = moduleName.split(PSEPARATOR);
		
		for(String m : parts) {
			if(m.trim().isEmpty()) {
				continue;
			}
			path += PSEPARATOR + m + MODULE_SUFFIX;
		}
		
		File module = new File(path);
		if(module.exists()) {
			if(module.isDirectory()) {
				return;
			} else {
				throwException("[ERROR]: module name overrides existing object - " + module.getAbsolutePath());
			}
		}
		
		module.mkdirs();
	}
	
	@Override
	public Document fetchDocument(String docuumentName, String revision) throws NoSuchObjectException {
		
		return null;
	}

	@Override
	public List<String> fetchDocumentRevisions(String docuumentName) throws NoSuchObjectException {
		
		return null;
	}

	@Override
	public String createNewDocument(String documentName, Document doc) throws RepositoryException {
		
		return null;
	}

	@Override
	public Document createDocumentBrench(String documentName, String baseRevision, Document newDocContent)
			throws RepositoryException, java.lang.SecurityException {
		
		
		return null;
	}
	
	@Override
	public Authorizable findUser(long sid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SecurityPrivileges fetchUserGeneralPrivileges(Authorizable user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SecurityPrivileges fetchUserPrivileges(Authorizable user, Document document) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SecurityPrivileges fetchUserPrivileges(Authorizable user, String module) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setGeneralRepositoryPrivileges(Authorizable initiator, Authorizable target, SecurityPrivileges sp)
			throws SecurityException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPrivileges(Authorizable initiator, Authorizable target, Document document, SecurityPrivileges sp)
			throws SecurityException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPrivileges(Authorizable initiator, Authorizable target, String module, SecurityPrivileges sp)
			throws SecurityException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setGroupPrivileges(Authorizable initiator, Authorizable target, Document document,
			SecurityPrivileges sp) throws SecurityException {
		// TODO Auto-generated method stub
		
	}
	
	private void attemptToLockReposytory(File locker) throws RepositoryException {
		
		String processName = ManagementFactory.getRuntimeMXBean().getName();
		if(locker.exists()) {
			try {
				FileUtils.forceDelete(locker);
			} catch (IOException e) {
				throwException("Failed to remove old repository lock file: ", e);
			}
		}
		
		try {
			FileUtils.touch(locker);
		} catch (IOException e) {
			throwException("Failed to create lock file: ", e);
		}
		
		try {
			FileUtils.write(locker, processName);
		} catch (IOException e) {
			throwException("Failed to create lock file: ", e);
		}
		
		locked = true;
	}
	
	private static void throwException(String message) throws RepositoryException {
		logger.error(message);
		throw new RepositoryException(message);
	}

	private static void throwException(String message, Exception e) throws RepositoryException {
		logger.error(message, e);
		throw new RepositoryException(message, e);
		
	}
	
	private String encodePath(String path) {
		try {
			return URLEncoder.encode(path, "UTF-8")
					.replaceAll("\\+", "%20")
					.replaceAll("\\%21", "!")
					.replaceAll("\\%27", "'")
					.replaceAll("\\%28", "(")
					.replaceAll("\\%29", ")")
					.replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}
	
	private String decodePath(String path) {
		try {
			return URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

}
