package com.crystalx.repository.file;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;

public final class FileUtilsEx {
	
	protected FileUtilsEx() {}

	public static boolean checkFileLock(File file) throws IOException {
		boolean locked = true;
		RandomAccessFile raf = new RandomAccessFile(file, "rw");
		FileChannel channel = raf.getChannel();
		FileLock lock = channel.lock();
		
		try {
		    lock = channel.tryLock();
		    locked = false;
		} catch (OverlappingFileLockException e) {
		    locked = true;
		} finally {
		    lock.release();
		}
		raf.close();
	    channel.close();
		return locked;
	}
}
