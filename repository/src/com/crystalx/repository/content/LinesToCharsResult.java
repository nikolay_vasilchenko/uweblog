package com.crystalx.repository.content;

import java.util.List;

public class LinesToCharsResult {
	
	protected String chars1;
	protected String chars2;
	protected List<String> lineArray;

	protected LinesToCharsResult(String chars1, String chars2, List<String> lineArray) {
		this.chars1 = chars1;
		this.chars2 = chars2;
		this.lineArray = lineArray;
	}
}
