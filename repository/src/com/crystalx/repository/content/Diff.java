package com.crystalx.repository.content;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;

/**
 * Class representing one diff operation.
 */
public class Diff {
	
	private static final Pattern BLANKLINEEND = Pattern.compile("\\n\\r?\\n\\Z", Pattern.DOTALL);
	private static final Pattern BLANKLINESTART = Pattern.compile("\\A\\r?\\n\\r?\\n", Pattern.DOTALL);
	
	/**
	 * Number of seconds to map a diff before giving up (0 for infinity).
	 */
	public static final float TIMEOUT = 1.0f;
	/**
	 * Cost of an empty edit operation in terms of edit characters.
	 */
	public static final short EDIT_COST = 4;
	/**
	 * The size beyond which the double-ended diff activates. Double-ending is
	 * twice as fast, but less accurate.
	 */
	public static final short DUAL_THRESHOLD = 32;

	protected Operation operation;
	protected String content;

	/**
	 * Constructor. Initializes the diff with the provided values.
	 * 
	 * @param operation One of INSERT, DELETE or EQUAL.
	 * @param content The text being applied.
	 */
	public Diff(Operation operation, String content) {
		this.operation = operation;
		this.content = content;
	}

	/**
	 * Display a human-readable version of this Diff.
	 * 
	 * @return text version.
	 */
	public String toString() {
		String prettyText = this.content.replace('\n', '\u00b6');
		return "Diff(" + this.operation + ",\"" + prettyText + "\")";
	}

	/**
	 * Is this Diff equivalent to another Diff?
	 * 
	 * @param d Another Diff to compare against.
	 * @return true or false.
	 */
	public boolean equals(Object d) {
		try {
			return (((Diff) d).operation == this.operation) && (((Diff) d).content.equals(this.content));
		} catch (ClassCastException e) {
			return false;
		}
	}
	
	public String getContent() {
		return content;
	}
	
	public Operation getOperation() {
		return operation;
	}
	
	public void updateContent(String content) {
		this.content = content;
	}
	
	public void toHtml(StringBuilder html, int elementIndex) {
		String text = content.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\n", "&para;<BR>");
		switch (operation) {
		case INSERT:
			html.append("<INS STYLE=\"background:#E6FFE6;\" TITLE=\"i=").append(elementIndex).append("\">").append(text).append("</INS>");
			break;
		case DELETE:
			html.append("<DEL STYLE=\"background:#FFE6E6;\" TITLE=\"i=").append(elementIndex).append("\">").append(text).append("</DEL>");
			break;
		case EQUAL:
			html.append("<SPAN TITLE=\"i=").append(elementIndex).append("\">").append(text).append("</SPAN>");
			break;
		}
	}
	
	//**********************************************
	// Utility methods
	
	/**
	 * Find the differences between two texts. Run a faster slightly less
	 * optimal diff This method allows the 'checklines' of diff_main() to be
	 * optional. Most of the time checklines is wanted, so default to true.
	 * 
	 * @param prevText Old string to be diffed.
	 * @param currText New string to be diffed.
	 * @return Linked List of Diff objects.
	 */
	public static List<Diff> find(String prevText, String currText) {
		return find(prevText, currText, true);
	}

	/**
	 * Find the differences between two texts. Simplifies the problem by
	 * stripping any common prefix or suffix off the texts before diffing.
	 * 
	 * @param prevText Old string to be diffed.
	 * @param currText New string to be diffed.
	 * @param checklines
	 *            Speedup flag. If false, then don't run a line-level diff first
	 *            to identify the changed areas. If true, then run a faster
	 *            slightly less optimal diff
	 * @return Linked List of Diff objects.
	 */
	public static List<Diff> find(String prevText, String currText, boolean checklines) {
		// Check for equality (speedup)
		List<Diff> diffs;
		if (prevText.equals(currText)) {
			diffs = new LinkedList<Diff>();
			diffs.add(new Diff(Operation.EQUAL, prevText));
			return diffs;
		}

		// Trim off common prefix (speedup)
		int commonLength = CommonStringUtils.computeCommonPrefix(prevText, currText);
		String commonPrefix = prevText.substring(0, commonLength);
		prevText = prevText.substring(commonLength);
		currText = currText.substring(commonLength);

		// Trim off common suffix (speedup)
		commonLength = CommonStringUtils.computeCommonSuffix(prevText, currText);
		String commonsuffix = prevText.substring(prevText.length() - commonLength);
		prevText = prevText.substring(0, prevText.length() - commonLength);
		currText = currText.substring(0, currText.length() - commonLength);

		// Compute the diff on the middle block
		diffs = compute(prevText, currText, checklines);

		// Restore the prefix and suffix
		if (commonPrefix.length() != 0) {
			diffs.add(0, new Diff(Operation.EQUAL, commonPrefix));
		}
		if (commonsuffix.length() != 0) {
			diffs.add(diffs.size(), new Diff(Operation.EQUAL, commonsuffix));
		}

		cleanupMerge(diffs);
		return diffs;
	}
	
	/**
	 * Reorder and merge like edit sections. Merge equalities. Any edit section
	 * can move as long as it doesn't cross an equality.
	 * 
	 * @param diffs LinkedList of Diff objects.
	 */
	public static void cleanupMerge(List<Diff> diffs) {
		
		// Add a dummy entry at theend.
		diffs.add(new Diff(Operation.EQUAL, ""));
		
		ListIterator<Diff> pointer = diffs.listIterator();
		int deletesCount = 0;
		int insertsCount = 0;
		String textToDelete = "";
		String textToInsert = "";
		Diff thisDiff = pointer.next();
		Diff prevEqual = null;
		int commonLength = 0;
		while (thisDiff != null) {
			switch (thisDiff.operation) {
			case INSERT:
				insertsCount++;
				textToInsert += thisDiff.content;
				prevEqual = null;
				break;
			case DELETE:
				deletesCount++;
				textToDelete += thisDiff.content;
				prevEqual = null;
				break;
			case EQUAL:
				if (deletesCount != 0 || insertsCount != 0) {
					// Delete the offending records.
					pointer.previous(); // Reverse direction.
					while (deletesCount-- > 0) {
						pointer.previous();
						pointer.remove();
					}
					while (insertsCount-- > 0) {
						pointer.previous();
						pointer.remove();
					}
					if (deletesCount != 0 && insertsCount != 0) {
						// Factor out any common prefixies.
						commonLength = CommonStringUtils.computeCommonPrefix(textToInsert, textToDelete);
						if (commonLength != 0) {
							if (pointer.hasPrevious()) {
								thisDiff = pointer.previous();
								assert thisDiff.operation == Operation.EQUAL : "Previous diff should have been an equality.";
								thisDiff.content += textToInsert.substring(0, commonLength);
								pointer.next();
							} else {
								pointer.add(new Diff(Operation.EQUAL, textToInsert.substring(0, commonLength)));
							}
							textToInsert = textToInsert.substring(commonLength);
							textToDelete = textToDelete.substring(commonLength);
						}
						// Factor out any common suffixies.
						commonLength = CommonStringUtils.computeCommonSuffix(textToInsert, textToDelete);
						if (commonLength != 0) {
							thisDiff = pointer.next();
							thisDiff.content = textToInsert.substring(textToInsert.length() - commonLength) + thisDiff.content;
							textToInsert = textToInsert.substring(0, textToInsert.length() - commonLength);
							textToDelete = textToDelete.substring(0, textToDelete.length() - commonLength);
							pointer.previous();
						}
					}
					// Insert the merged records.
					if (textToDelete.length() != 0) {
						pointer.add(new Diff(Operation.DELETE, textToDelete));
					}
					if (textToInsert.length() != 0) {
						pointer.add(new Diff(Operation.INSERT, textToInsert));
					}
					// Step forward to the equality.
					thisDiff = pointer.hasNext() ? pointer.next() : null;
				} else if (prevEqual != null) {
					// Merge this equality with the previous one.
					prevEqual.content += thisDiff.content;
					pointer.remove();
					thisDiff = pointer.previous();
					pointer.next(); // Forward direction
				}
				insertsCount = 0;
				deletesCount = 0;
				textToDelete = "";
				textToInsert = "";
				prevEqual = thisDiff;
				break;
			}
			thisDiff = pointer.hasNext() ? pointer.next() : null;
		}
		// System.out.println(diff);
		if (diffs.get(diffs.size()-1).content.length() == 0) {
			//diffs.removeLast(); // Remove the dummy entry at the end.
			diffs.remove(diffs.size() - 1);
		}

		/*
		 * Second pass: look for single edits surrounded on both sides by
		 * equalities which can be shifted sideways to eliminate an equality.
		 * e.g: A<ins>BA</ins>C -> <ins>AB</ins>AC
		 */
		boolean hasChanges = false;
		// Create a new iterator at the start.
		// (As opposed to walking the current one back.)
		pointer = diffs.listIterator();
		Diff prevDiff = pointer.hasNext() ? pointer.next() : null;
		thisDiff = pointer.hasNext() ? pointer.next() : null;
		Diff nextDiff = pointer.hasNext() ? pointer.next() : null;
		// Intentionally ignore the first and last element (don't need checking).
		while (nextDiff != null) {
			if (prevDiff.operation == Operation.EQUAL && nextDiff.operation == Operation.EQUAL) {
				// This is a single edit surrounded by equalities.
				if (thisDiff.content.endsWith(prevDiff.content)) {
					// Shift the edit over the previous equality.
					thisDiff.content = prevDiff.content
							+ thisDiff.content.substring(0, thisDiff.content.length() - prevDiff.content.length());
					nextDiff.content = prevDiff.content + nextDiff.content;
					pointer.previous(); // Walk past nextDiff.
					pointer.previous(); // Walk past thisDiff.
					pointer.previous(); // Walk past prevDiff.
					pointer.remove(); // Delete prevDiff.
					pointer.next(); // Walk past thisDiff.
					thisDiff = pointer.next(); // Walk past nextDiff.
					nextDiff = pointer.hasNext() ? pointer.next() : null;
					hasChanges = true;
				} else if (thisDiff.content.startsWith(nextDiff.content)) {
					// Shift the edit over the next equality.
					prevDiff.content += nextDiff.content;
					thisDiff.content = thisDiff.content.substring(nextDiff.content.length()) + nextDiff.content;
					pointer.remove(); // Delete nextDiff.
					nextDiff = pointer.hasNext() ? pointer.next() : null;
					hasChanges = true;
				}
			}
			prevDiff = thisDiff;
			thisDiff = nextDiff;
			nextDiff = pointer.hasNext() ? pointer.next() : null;
		}
		// If shifts were made, the diff needs reordering and another shift
		// sweep.
		if (hasChanges) {
			cleanupMerge(diffs);
		}
	}
	
	/**
	 * Compute the equivalent location in text2.
	 * E.g. "The cat" vs "The big cat", 1->1, 5->8
	 * 
	 * @param diffs list of Diff objects.
	 * @param loc location within text1.
	 * @return Location within text2.
	 */
	public static int xIndex(List<Diff> diffs, int loc) {
		int chars1 = 0;
		int chars2 = 0;
		int lastChars1 = 0;
		int lastChars2 = 0;
		Diff lastDiff = null;
		for (Diff aDiff : diffs) {
			if (aDiff.operation != Operation.INSERT) {
				// Equality or deletion.
				chars1 += aDiff.content.length();
			}
			if (aDiff.operation != Operation.DELETE) {
				// Equality or insertion.
				chars2 += aDiff.content.length();
			}
			if (chars1 > loc) {
				// Overshot the location.
				lastDiff = aDiff;
				break;
			}
			lastChars1 = chars1;
			lastChars2 = chars2;
		}
		if (lastDiff != null && lastDiff.operation == Operation.DELETE) {
			// The location was deleted.
			return lastChars2;
		}
		// Add the remaining character length.
		return lastChars2 + (loc - lastChars1);
	}

	/**
	 * Convert a Diff list into a pretty HTML report.
	 * 
	 * @param diffs
	 *            LinkedList of Diff objects.
	 * @return HTML representation.
	 */
	public static String prettyHtml(List<Diff> diffs) {
		StringBuilder html = new StringBuilder();
		int i = 0;
		for (Diff aDiff : diffs) {
			aDiff.toHtml(html, i);
			if (aDiff.operation != Operation.DELETE) {
				i += aDiff.content.length();
			}
		}
		return html.toString();
	}

	/**
	 * Compute and return the source text (all equalities and deletions).
	 * 
	 * @param diffs LinkedList of Diff objects.
	 * @return Source text.
	 */
	public static String computeSource(List<Diff> diffs) {
		StringBuilder text = new StringBuilder();
		for (Diff diff : diffs) {
			if (diff.operation != Operation.INSERT) {
				text.append(diff.content);
			}
		}
		return text.toString();
	}

	/**
	 * Compute and return the destination text (all equalities and insertions).
	 * 
	 * @param diffs LinkedList of Diff objects.
	 * @return Destination text.
	 */
	public static String computeDestination(List<Diff> diffs) {
		StringBuilder text = new StringBuilder();
		for (Diff diff : diffs) {
			if (diff.operation != Operation.DELETE) {
				text.append(diff.content);
			}
		}
		return text.toString();
	}

	/**
	 * Compute the Levenshtein distance; the number of inserted, deleted or
	 * substituted characters.
	 * 
	 * @param diffs
	 *            LinkedList of Diff objects.
	 * @return Number of changes.
	 */
	public static int levenshtein(List<Diff> diffs) {
		int levenshtein = 0;
		int insertions = 0;
		int deletions = 0;
		for (Diff aDiff : diffs) {
			switch (aDiff.operation) {
			case INSERT:
				insertions += aDiff.content.length();
				break;
			case DELETE:
				deletions += aDiff.content.length();
				break;
			case EQUAL:
				// A deletion and an insertion is one substitution.
				levenshtein += Math.max(insertions, deletions);
				insertions = 0;
				deletions = 0;
				break;
			}
		}
		levenshtein += Math.max(insertions, deletions);
		return levenshtein;
	}

	/**
	 * Crush the diff into an encoded string which describes the operations
	 * required to transform text1 into text2. E.g. =3\t-2\t+ing -> Keep 3
	 * chars, delete 2 chars, insert 'ing'. Operations are tab-separated.
	 * Inserted text is escaped using %xx notation.
	 * 
	 * @param diffs
	 *            Array of diff tuples.
	 * @return Delta text.
	 */
	public static String toDelta(List<Diff> diffs) {
		StringBuilder text = new StringBuilder();
		for (Diff aDiff : diffs) {
			switch (aDiff.operation) {
			case INSERT:
				try {
					text.append("+").append(URLEncoder.encode(aDiff.content, "UTF-8").replace('+', ' ')).append("\t");
				} catch (UnsupportedEncodingException e) {
					// Not likely on modern system.
					throw new Error("This system does not support UTF-8.", e);
				}
				break;
			case DELETE:
				text.append("-").append(aDiff.content.length()).append("\t");
				break;
			case EQUAL:
				text.append("=").append(aDiff.content.length()).append("\t");
				break;
			}
		}
		String delta = text.toString();
		if (delta.length() != 0) {
			// Strip off trailing tab character.
			delta = delta.substring(0, delta.length() - 1);
			delta = CommonStringUtils.unescapeForEncodeUriCompatability(delta);
		}
		return delta;
	}

	/**
	 * Given the original text1, and an encoded string which describes the
	 * operations required to transform text1 into text2, compute the full diff.
	 * 
	 * @param source
	 *            Source string for the diff.
	 * @param delta
	 *            Delta text.
	 * @return Array of diff tuples or null if invalid.
	 * @throws IllegalArgumentException
	 *             If invalid input.
	 */
	public static List<Diff> fromDelta(String source, String delta) throws IllegalArgumentException {
		List<Diff> diffs = new LinkedList<Diff>();
		int pointer = 0; // Cursor in text1
		String[] tokens = delta.split("\t");
		for (String token : tokens) {
			if (token.length() == 0) {
				// Blank tokens are ok (from a trailing \t).
				continue;
			}
			// Each token begins with a one character parameter which specifies
			// the
			// operation of this token (delete, insert, equality).
			String param = token.substring(1);
			switch (token.charAt(0)) {
			case '+':
				// decode would change all "+" to " "
				param = param.replace("+", "%2B");
				try {
					param = URLDecoder.decode(param, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// Not likely on modern system.
					throw new Error("This system does not support UTF-8.", e);
				} catch (IllegalArgumentException e) {
					// Malformed URI sequence.
					throw new IllegalArgumentException("Illegal escape in diff_fromDelta: " + param, e);
				}
				diffs.add(new Diff(Operation.INSERT, param));
				break;
			case '-':
				// Fall through.
			case '=':
				int n;
				try {
					n = Integer.parseInt(param);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Invalid number in diff_fromDelta: " + param, e);
				}
				if (n < 0) {
					throw new IllegalArgumentException("Negative number in diff_fromDelta: " + param);
				}
				String text;
				try {
					text = source.substring(pointer, pointer += n);
				} catch (StringIndexOutOfBoundsException e) {
					throw new IllegalArgumentException(
							"Delta length (" + pointer + ") larger than source text length (" + source.length() + ").",
							e);
				}
				if (token.charAt(0) == '=') {
					diffs.add(new Diff(Operation.EQUAL, text));
				} else {
					diffs.add(new Diff(Operation.DELETE, text));
				}
				break;
			default:
				// Anything else is an error.
				throw new IllegalArgumentException("Invalid diff operation in diff_fromDelta: " + token.charAt(0));
			}
		}
		if (pointer != source.length()) {
			throw new IllegalArgumentException(
					"Delta length (" + pointer + ") smaller than source text length (" + source.length() + ").");
		}
		return diffs;
	}
	
	/**
	 * Reduce the number of edits by eliminating operationally trivial
	 * equalities.
	 * 
	 * @param diffsLinkedList of Diff objects.
	 */
	
	public static void cleanupEfficiency(List<Diff> diffs) {
		
		if (diffs.isEmpty()) {
			return;
		}
		boolean changes = false;
		Stack<Diff> equalities = new Stack<Diff>(); // Stack of equalities.
		String lastEquality = null; // Always equal to equalities.lastElement().text
		ListIterator<Diff> pointer = diffs.listIterator();
		
		// Is there an insertion operation before the last equality.
		boolean preIns = false;
		// Is there a deletion operation before the last equality.
		boolean preDel = false;
		// Is there an insertion operation after the last equality.
		boolean postIns = false;
		// Is there a deletion operation after the last equality.
		boolean postDel = false;
		Diff thisDiff = pointer.next();
		Diff safeDiff = thisDiff; // The last Diff that is known to be unsplitable.
		
		while (thisDiff != null) {
			if (thisDiff.operation == Operation.EQUAL) {
				// equality found
				if (thisDiff.content.length() < EDIT_COST && (postIns || postDel)) {
					// Candidate found.
					equalities.push(thisDiff);
					preIns = postIns;
					preDel = postDel;
					lastEquality = thisDiff.content;
				} else {
					// Not a candidate, and can never become one.
					equalities.clear();
					lastEquality = null;
					safeDiff = thisDiff;
				}
				postIns = postDel = false;
			} else {
				// an insertion or deletion
				if (thisDiff.operation == Operation.DELETE) {
					postDel = true;
				} else {
					postIns = true;
				}
				/*
				 * Five types to be split:
				 * <ins>A</ins><del>B</del>XY<ins>C</ins><del>D</del>
				 * <ins>A</ins>X<ins>C</ins><del>D</del>
				 * <ins>A</ins><del>B</del>X<ins>C</ins>
				 * <ins>A</del>X<ins>C</ins><del>D</del>
				 * <ins>A</ins><del>B</del>X<del>C</del>
				 */
				if (lastEquality != null && ((preIns && preDel && postIns && postDel)
						|| ((lastEquality.length() < EDIT_COST / 2) && ((preIns ? 1 : 0) + (preDel ? 1 : 0)
								+ (postIns ? 1 : 0) + (postDel ? 1 : 0)) == 3))) {
					// System.out.println("Splitting: '" + lastequality + "'");
					// Walk back to offending equality.
					while (thisDiff != equalities.lastElement()) {
						thisDiff = pointer.previous();
					}
					pointer.next();

					// Replace equality with a delete.
					pointer.set(new Diff(Operation.DELETE, lastEquality));
					// Insert a corresponding an insert.
					pointer.add(thisDiff = new Diff(Operation.INSERT, lastEquality));

					equalities.pop(); // Throw away the equality we just deleted.
					lastEquality = null;
					if (preIns && preDel) {
						// No changes made which could affect previous entry, keep going.
						postIns = postDel = true;
						equalities.clear();
						safeDiff = thisDiff;
					} else {
						if (!equalities.empty()) {
							// Throw away the previous equality (it needs to be reevaluated).
							equalities.pop();
						}
						if (equalities.empty()) {
							// There are no previous questionable equalities,
							// walk back to the last known safe diff.
							thisDiff = safeDiff;
						} else {
							// There is an equality we can fall back to.
							thisDiff = equalities.lastElement();
						}
						while (thisDiff != pointer.previous()) {
							// Intentionally empty loop.
						}
						postIns = postDel = false;
					}

					changes = true;
				}
			}
			thisDiff = pointer.hasNext() ? pointer.next() : null;
		}

		if (changes) {
			cleanupMerge(diffs);
		}
	}
	
	/**
	 * Reduce the number of edits by eliminating semantically trivial
	 * equalities.
	 * 
	 * @param diffs
	 *            LinkedList of Diff objects.
	 */
	public static void cleanupSemantic(List<Diff> diffs) {
		if (diffs.isEmpty()) {
			return;
		}
		boolean changes = false;
		Stack<Diff> equalities = new Stack<Diff>(); // Stack of qualities.
		String lastEquality = null; // Always equal to equalities.lastElement().text
		ListIterator<Diff> pointer = diffs.listIterator();
		// Number of characters that changed prior to the equality.
		int beforeChangesLength = 0;
		// Number of characters that changed after the equality.
		int afterChangesLength = 0;
		Diff thisDiff = pointer.next();
		while (thisDiff != null) {
			if (thisDiff.operation == Operation.EQUAL) {
				// equality found
				equalities.push(thisDiff);
				beforeChangesLength = afterChangesLength;
				afterChangesLength = 0;
				lastEquality = thisDiff.content;
			} else {
				// an insertion or deletion
				afterChangesLength += thisDiff.content.length();
				if (lastEquality != null && (lastEquality.length() <= beforeChangesLength)
						&& (lastEquality.length() <= afterChangesLength)) {
					// System.out.println("Splitting: '" + lastequality + "'");
					// Walk back to offending equality.
					while (thisDiff != equalities.lastElement()) {
						thisDiff = pointer.previous();
					}
					pointer.next();

					// Replace equality with a delete.
					pointer.set(new Diff(Operation.DELETE, lastEquality));
					// Insert a corresponding an insert.
					pointer.add(new Diff(Operation.INSERT, lastEquality));

					equalities.pop(); // Throw away the equality we just
										// deleted.
					if (!equalities.empty()) {
						// Throw away the previous equality (it needs to be reevaluated).
						equalities.pop();
					}
					if (equalities.empty()) {
						// There are no previous equalities, walk back to the start.
						while (pointer.hasPrevious()) {
							pointer.previous();
						}
					} else {
						// There is a safe equality we can fall back to.
						thisDiff = equalities.lastElement();
						while (thisDiff != pointer.previous()) {
							// Intentionally empty loop.
						}
					}

					beforeChangesLength = 0; // Reset the counters.
					afterChangesLength = 0;
					lastEquality = null;
					changes = true;
				}
			}
			thisDiff = pointer.hasNext() ? pointer.next() : null;
		}

		if (changes) {
			cleanupMerge(diffs);
		}
		cleanupSemanticLossless(diffs);
	}

	/**
	 * Look for single edits surrounded on both sides by equalities which can be
	 * shifted sideways to align the edit to a word boundary. e.g: The c<ins>at
	 * c</ins>ame. -> The <ins>cat </ins>came.
	 * 
	 * @param diffs
	 *            LinkedList of Diff objects.
	 */
	public static void cleanupSemanticLossless(List<Diff> diffs) {
		
		String equality1, edit, equality2;
		String commonString;
		int commonOffset;
		int score, bestScore;
		String bestEquality1, bestEdit, bestEquality2;
		
		// Create a new iterator at the start.
		ListIterator<Diff> pointer = diffs.listIterator();
		Diff prevDiff = pointer.hasNext() ? pointer.next() : null;
		Diff thisDiff = pointer.hasNext() ? pointer.next() : null;
		Diff nextDiff = pointer.hasNext() ? pointer.next() : null;
		
		// Intentionally ignore the first and last element (don't need checking).
		while (nextDiff != null) {
			if (prevDiff.operation == Operation.EQUAL && nextDiff.operation == Operation.EQUAL) {
				// This is a single edit surrounded by equalities.
				equality1 = prevDiff.content;
				edit = thisDiff.content;
				equality2 = nextDiff.content;

				// First, shift the edit as far left as possible.
				commonOffset = CommonStringUtils.computeCommonSuffix(equality1, edit);
				if (commonOffset != 0) {
					commonString = edit.substring(edit.length() - commonOffset);
					equality1 = equality1.substring(0, equality1.length() - commonOffset);
					edit = commonString + edit.substring(0, edit.length() - commonOffset);
					equality2 = commonString + equality2;
				}

				// Second, step character by character right, looking for the best fit.
				bestEquality1 = equality1;
				bestEdit = edit;
				bestEquality2 = equality2;
				bestScore = cleanupSemanticScore(equality1, edit) + cleanupSemanticScore(edit, equality2);
				while (edit.length() != 0 && equality2.length() != 0 && edit.charAt(0) == equality2.charAt(0)) {
					equality1 += edit.charAt(0);
					edit = edit.substring(1) + equality2.charAt(0);
					equality2 = equality2.substring(1);
					score = cleanupSemanticScore(equality1, edit) + cleanupSemanticScore(edit, equality2);
					// The >= encourages trailing rather than leading whitespace
					// on edits.
					if (score >= bestScore) {
						bestScore = score;
						bestEquality1 = equality1;
						bestEdit = edit;
						bestEquality2 = equality2;
					}
				}

				if (!prevDiff.content.equals(bestEquality1)) {
					// We have an improvement, save it back to the diff.
					if (bestEquality1.length() != 0) {
						prevDiff.content = bestEquality1;
					} else {
						pointer.previous(); // Walk past nextDiff.
						pointer.previous(); // Walk past thisDiff.
						pointer.previous(); // Walk past prevDiff.
						pointer.remove(); // Delete prevDiff.
						pointer.next(); // Walk past thisDiff.
						pointer.next(); // Walk past nextDiff.
					}
					thisDiff.content = bestEdit;
					if (bestEquality2.length() != 0) {
						nextDiff.content = bestEquality2;
					} else {
						pointer.remove(); // Delete nextDiff.
						nextDiff = thisDiff;
						thisDiff = prevDiff;
					}
				}
			}
			prevDiff = thisDiff;
			thisDiff = nextDiff;
			nextDiff = pointer.hasNext() ? pointer.next() : null;
		}
	}
	
	/**
	 * Find the differences between two texts. Assumes that the texts do not
	 * have any common prefix or suffix.
	 * 
	 * @param text1 Old string to be diffed.
	 * @param text2 New string to be diffed.
	 * @param checklines
	 *            Speedup flag. If false, then don't run a line-level diff first
	 *            to identify the changed areas. If true, then run a faster
	 *            slightly less optimal diff
	 * @return Linked List of Diff objects.
	 */
	protected static List<Diff> compute(String text1, String text2, boolean checklines) {
		List<Diff> diffs = new LinkedList<Diff>();

		if (text1.length() == 0) {
			// Just add some text (speedup)
			diffs.add(new Diff(Operation.INSERT, text2));
			return diffs;
		}

		if (text2.length() == 0) {
			// Just delete some text (speedup)
			diffs.add(new Diff(Operation.DELETE, text1));
			return diffs;
		}

		String longtext = text1.length() > text2.length() ? text1 : text2;
		String shorttext = text1.length() > text2.length() ? text2 : text1;
		int i = longtext.indexOf(shorttext);
		if (i != -1) {
			// Shorter text is inside the longer text (speedup)
			Operation op = (text1.length() > text2.length()) ? Operation.DELETE : Operation.INSERT;
			diffs.add(new Diff(op, longtext.substring(0, i)));
			diffs.add(new Diff(Operation.EQUAL, shorttext));
			diffs.add(new Diff(op, longtext.substring(i + shorttext.length())));
			return diffs;
		}
		longtext = shorttext = null; // Garbage collect.

		// Check to see if the problem can be split in two.
		String[] hm = halfMatch(text1, text2);
		if (hm != null) {
			// A half-match was found, sort out the return data.
			String text1_a = hm[0];
			String text1_b = hm[1];
			String text2_a = hm[2];
			String text2_b = hm[3];
			String mid_common = hm[4];
			// Send both pairs off for separate processing.
			List<Diff> diffs_a = find(text1_a, text2_a, checklines);
			List<Diff> diffs_b = find(text1_b, text2_b, checklines);
			// Merge the results.
			diffs = diffs_a;
			diffs.add(new Diff(Operation.EQUAL, mid_common));
			diffs.addAll(diffs_b);
			return diffs;
		}

		// Perform a real diff.
		if (checklines && (text1.length() < 100 || text2.length() < 100)) {
			checklines = false; // Too trivial for the overhead.
		}
		List<String> linearray = null;
		if (checklines) {
			// Scan the text on a line-by-line basis first.
			LinesToCharsResult b = LinesUtils.linesToChars(text1, text2);
			text1 = b.chars1;
			text2 = b.chars2;
			linearray = b.lineArray;
		}

		diffs = intersection(text1, text2);
		if (diffs == null) {
			// No acceptable result.
			diffs = new LinkedList<Diff>();
			diffs.add(new Diff(Operation.DELETE, text1));
			diffs.add(new Diff(Operation.INSERT, text2));
		}

		if (checklines) {
			// Convert the diff back to original text.
			LinesUtils.charsToLines(diffs, linearray);
			// Eliminate freak matches (e.g. blank lines)
			cleanupSemantic(diffs);

			// Rediff any replacement blocks, this time character-by-character.
			// Add a dummy entry at the end.
			diffs.add(new Diff(Operation.EQUAL, ""));
			int count_delete = 0;
			int count_insert = 0;
			String text_delete = "";
			String text_insert = "";
			ListIterator<Diff> pointer = diffs.listIterator();
			Diff thisDiff = pointer.next();
			while (thisDiff != null) {
				switch (thisDiff.operation) {
				case INSERT:
					count_insert++;
					text_insert += thisDiff.content;
					break;
				case DELETE:
					count_delete++;
					text_delete += thisDiff.content;
					break;
				case EQUAL:
					// Upon reaching an equality, check for prior redundancies.
					if (count_delete >= 1 && count_insert >= 1) {
						// Delete the offending records and add the merged ones.
						pointer.previous();
						for (int j = 0; j < count_delete + count_insert; j++) {
							pointer.previous();
							pointer.remove();
						}
						for (Diff newDiff : find(text_delete, text_insert, false)) {
							pointer.add(newDiff);
						}
					}
					count_insert = 0;
					count_delete = 0;
					text_delete = "";
					text_insert = "";
					break;
				}
				thisDiff = pointer.hasNext() ? pointer.next() : null;
			}
			//diffs.removeLast(); // Remove the dummy entry at the end.
			diffs.remove(diffs.size() - 1);
		}
		return diffs;
	}
	
	/**
	 * Do the two texts share a substring which is at least half the length of
	 * the longer text?
	 * 
	 * @param text1 First string.
	 * @param text2 Second string.
	 * @return Five element String array, containing the prefix of text1, the
	 *         suffix of text1, the prefix of text2, the suffix of text2 and the
	 *         common middle. Or null if there was no match.
	 */
	protected static String[] halfMatch(String text1, String text2) {
		String longtext = text1.length() > text2.length() ? text1 : text2;
		String shorttext = text1.length() > text2.length() ? text2 : text1;
		if (longtext.length() < 10 || shorttext.length() < 1) {
			return null; // Pointless.
		}

		// First check if the second quarter is the seed for a half-match.
		String[] hm1 = halfMatch(longtext, shorttext, (longtext.length() + 3) / 4);
		// Check again based on the third quarter.
		String[] hm2 = halfMatch(longtext, shorttext, (longtext.length() + 1) / 2);
		String[] hm;
		if (hm1 == null && hm2 == null) {
			return null;
		} else if (hm2 == null) {
			hm = hm1;
		} else if (hm1 == null) {
			hm = hm2;
		} else {
			// Both matched. Select the longest.
			hm = hm1[4].length() > hm2[4].length() ? hm1 : hm2;
		}

		// A half-match was found, sort out the return data.
		if (text1.length() > text2.length()) {
			return hm;
			// return new String[]{hm[0], hm[1], hm[2], hm[3], hm[4]};
		} else {
			return new String[] { hm[2], hm[3], hm[0], hm[1], hm[4] };
		}
	}
	
	/**
	 * Does a substring of shorttext exist within longtext such that the
	 * substring is at least half the length of longtext?
	 * 
	 * @param longtext Longer string.
	 * @param shorttext Shorter string.
	 * @param index
	 *            Start index of quarter length substring within longtext.
	 * @return Five element String array, containing the prefix of longtext, the
	 *         suffix of longtext, the prefix of shorttext, the suffix of
	 *         shorttext and the common middle. Or null if there was no match.
	 */
	private static String[] halfMatch(String longtext, String shorttext, int index) {
		// Start with a 1/4 length substring at position i as a seed.
		String seed = longtext.substring(index, index + longtext.length() / 4);
		int j = -1;
		String best_common = "";
		String best_longtext_a = "", best_longtext_b = "";
		String best_shorttext_a = "", best_shorttext_b = "";
		while ((j = shorttext.indexOf(seed, j + 1)) != -1) {
			int prefixLength = CommonStringUtils.computeCommonPrefix(longtext.substring(index), shorttext.substring(j));
			int suffixLength = CommonStringUtils.computeCommonSuffix(longtext.substring(0, index), shorttext.substring(0, j));
			if (best_common.length() < suffixLength + prefixLength) {
				best_common = shorttext.substring(j - suffixLength, j) + shorttext.substring(j, j + prefixLength);
				best_longtext_a = longtext.substring(0, index - suffixLength);
				best_longtext_b = longtext.substring(index + prefixLength);
				best_shorttext_a = shorttext.substring(0, j - suffixLength);
				best_shorttext_b = shorttext.substring(j + prefixLength);
			}
		}
		if (best_common.length() >= longtext.length() / 2) {
			return new String[] { best_longtext_a, best_longtext_b, best_shorttext_a, best_shorttext_b, best_common };
		} else {
			return null;
		}
	}
	
	/**
	 * Explore the intersection points between the two texts.
	 * 
	 * @param previous Old string to be diffed.
	 * @param current New string to be diffed.
	 * @return LinkedList of Diff objects or null if no diff available.
	 */
	protected static List<Diff> intersection(String previous, String current) {
		
		long endTime = System.currentTimeMillis() + (long) (TIMEOUT * 1000);
		// Cache the text lengths to prevent multiple calls.
		
		int prevLength = previous.length();
		int currLength = current.length();
		int max_d = prevLength + currLength - 1;
		boolean doubleEnd = DUAL_THRESHOLD * 2 < max_d;
		
		List<Set<Long>> v_map1 = new ArrayList<Set<Long>>();
		List<Set<Long>> v_map2 = new ArrayList<Set<Long>>();
		Map<Integer, Integer> v1 = new HashMap<Integer, Integer>();
		Map<Integer, Integer> v2 = new HashMap<Integer, Integer>();
		
		v1.put(1, 0);
		v2.put(1, 0);
		
		int x, y;
		Long footstep = 0L; // Used to track overlapping paths.
		Map<Long, Integer> footsteps = new HashMap<Long, Integer>();
		boolean done = false;
		
		// If the total number of characters is odd, then the front path will
		// collide with the reverse path.
		boolean front = ((prevLength + currLength) % 2 == 1);
		
		for (int d = 0; d < max_d; d++) {
			// Bail out if timeout reached.
			if (TIMEOUT > 0 && System.currentTimeMillis() > endTime) {
				return null;
			}

			// Walk the front path one step.
			v_map1.add(new HashSet<Long>()); // Adds at index 'd'.
			for (int k = -d; k <= d; k += 2) {
				if (k == -d || k != d && v1.get(k - 1) < v1.get(k + 1)) {
					x = v1.get(k + 1);
				} else {
					x = v1.get(k - 1) + 1;
				}
				y = x - k;
				if (doubleEnd) {
					footstep = CommonUtils.footprint(x, y);
					if (front && (footsteps.containsKey(footstep))) {
						done = true;
					}
					if (!front) {
						footsteps.put(footstep, d);
					}
				}
				while (!done && x < prevLength && y < currLength && previous.charAt(x) == current.charAt(y)) {
					x++;
					y++;
					if (doubleEnd) {
						footstep = CommonUtils.footprint(x, y);
						if (front && (footsteps.containsKey(footstep))) {
							done = true;
						}
						if (!front) {
							footsteps.put(footstep, d);
						}
					}
				}
				v1.put(k, x);
				v_map1.get(d).add(CommonUtils.footprint(x, y));
				if (x == prevLength && y == currLength) {
					// Reached the end in single-path mode.
					return PathUtils.middleBackwardPath(v_map1, previous, current);
				} else if (done) {
					// Front path ran over reverse path.
					v_map2 = v_map2.subList(0, footsteps.get(footstep) + 1);
					List<Diff> a = PathUtils.middleBackwardPath(v_map1, previous.substring(0, x), current.substring(0, y));
					a.addAll(PathUtils.middleForvardPath(v_map2, previous.substring(x), current.substring(y)));
					return a;
				}
			}

			if (doubleEnd) {
				// Walk the reverse path one step.
				v_map2.add(new HashSet<Long>()); // Adds at index 'd'.
				for (int k = -d; k <= d; k += 2) {
					if (k == -d || k != d && v2.get(k - 1) < v2.get(k + 1)) {
						x = v2.get(k + 1);
					} else {
						x = v2.get(k - 1) + 1;
					}
					y = x - k;
					footstep = CommonUtils.footprint(prevLength - x, currLength - y);
					if (!front && (footsteps.containsKey(footstep))) {
						done = true;
					}
					if (front) {
						footsteps.put(footstep, d);
					}
					while (!done && x < prevLength && y < currLength
							&& previous.charAt(prevLength - x - 1) == current.charAt(currLength - y - 1)) {
						x++;
						y++;
						footstep = CommonUtils.footprint(prevLength - x, currLength - y);
						if (!front && (footsteps.containsKey(footstep))) {
							done = true;
						}
						if (front) {
							footsteps.put(footstep, d);
						}
					}
					v2.put(k, x);
					v_map2.get(d).add(CommonUtils.footprint(x, y));
					if (done) {
						// Reverse path ran over front path.
						v_map1 = v_map1.subList(0, footsteps.get(footstep) + 1);
						List<Diff> a = PathUtils.middleBackwardPath(v_map1, previous.substring(0, prevLength - x),
								current.substring(0, currLength - y));
						a.addAll(PathUtils.middleForvardPath(v_map2, previous.substring(prevLength - x),
								current.substring(currLength - y)));
						return a;
					}
				}
			}
		}
		// Number of diffs equals number of characters, no commonality at all.
		return null;
	}
	
	

	/**
	 * Given two strings, compute a score representing whether the internal
	 * boundary falls on logical boundaries. Scores range from 5 (best) to 0
	 * (worst).
	 * 
	 * @param one
	 *            First string.
	 * @param two
	 *            Second string.
	 * @return The score.
	 */
	private static int cleanupSemanticScore(String one, String two) {
		if (one.length() == 0 || two.length() == 0) {
			// Edges are the best.
			return 5;
		}

		// Each port of this function behaves slightly differently due to
		// subtle differences in each language's definition of things like
		// 'whitespace'. Since this function's purpose is largely cosmetic,
		// the choice has been made to use each language's native features
		// rather than force total conformity.
		int score = 0;
		// One point for non-alphanumeric.
		if (!Character.isLetterOrDigit(one.charAt(one.length() - 1)) || !Character.isLetterOrDigit(two.charAt(0))) {
			score++;
			// Two points for whitespace.
			if (Character.isWhitespace(one.charAt(one.length() - 1)) || Character.isWhitespace(two.charAt(0))) {
				score++;
				// Three points for line breaks.
				if (Character.getType(one.charAt(one.length() - 1)) == Character.CONTROL
						|| Character.getType(two.charAt(0)) == Character.CONTROL) {
					score++;
					// Four points for blank lines.
					if (BLANKLINEEND.matcher(one).find() || BLANKLINESTART.matcher(two).find()) {
						score++;
					}
				}
			}
		}
		return score;
	}
	
}