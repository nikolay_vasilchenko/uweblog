package com.crystalx.repository.content;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Patch {

	/**
	 * When deleting a large block of text (over ~64 characters), how close does
	 * the contents have to match the expected contents. (0.0 = perfection, 1.0
	 * = very loose). Note that Match_Threshold controls how closely the end
	 * points of a delete need to match.
	 */
	public static final float DELETE_THRESHOLD = 0.5f;
	
	/**
	 * Chunk size for context length.
	 */
	public static final short MARGIN = 4;
	
	public static final Pattern PATCH_HEADER = Pattern.compile("^@@ -(\\d+),?(\\d*) \\+(\\d+),?(\\d*) @@$");
	
	protected LinkedList<Diff> diffs = new LinkedList<Diff>();
	protected int start1 = 0;
	protected int start2 = 0;
	protected int length1 = 0;
	protected int length2 = 0;


    public Patch() {

    }
    /**
     * Emmulate GNU diff's format.
     * Header: @@ -382,8 +481,9 @@
     * Indicies are printed as 1-based, not 0-based.
     * @return The GNU diff string.
     */
    public String toString() {
      String coords1, coords2;
      if (this.length1 == 0) {
        coords1 = this.start1 + ",0";
      } else if (this.length1 == 1) {
        coords1 = Integer.toString(this.start1 + 1);
      } else {
        coords1 = (this.start1 + 1) + "," + this.length1;
      }
      if (this.length2 == 0) {
        coords2 = this.start2 + ",0";
      } else if (this.length2 == 1) {
        coords2 = Integer.toString(this.start2 + 1);
      } else {
        coords2 = (this.start2 + 1) + "," + this.length2;
      }
      StringBuilder text = new StringBuilder();
      text.append("@@ -").append(coords1).append(" +").append(coords2)
          .append(" @@\n");
      // Escape the body of the patch with %xx notation.
      for (Diff diff : this.diffs) {
        switch (diff.operation) {
        case INSERT:
          text.append('+');
          break;
        case DELETE:
          text.append('-');
          break;
        case EQUAL:
          text.append(' ');
          break;
        }
        try {
          text.append(URLEncoder.encode(diff.content, "UTF-8").replace('+', ' '))
              .append("\n");
        } catch (UnsupportedEncodingException e) {
          // Not likely on modern system.
          throw new Error("This system does not support UTF-8.", e);
        }
      }
      return CommonStringUtils.unescapeForEncodeUriCompatability(text.toString());
    }
    
    //******************************************************************
    // Utils methods
    
	/**
	 * Compute a list of patches to turn text1 into text2. A set of diffs will
	 * be computed.
	 * 
	 * @param previous Old text.
	 * @param current New text.
	 * @return LinkedList of Patch objects.
	 */
	public static List<Patch> compute(String previous, String current) {
		// No diffs provided, compute our own.
		List<Diff> diffs = Diff.find(previous, current, true);
		if (diffs.size() > 2) {
			Diff.cleanupSemantic(diffs);
			Diff.cleanupEfficiency(diffs);
		}
		return compute(previous, diffs);
	}

	/**
	 * Compute a list of patches to turn text1 into text2. text1 will be derived
	 * from the provided diffs.
	 * 
	 * @param diffs Array of diff tuples for text1 to text2.
	 * @return LinkedList of Patch objects.
	 */
	public List<Patch> compute(List<Diff> diffs) {
		// No origin string provided, compute our own.
		String text = Diff.computeSource(diffs);
		return compute(text, diffs);
	}

	/**
	 * Compute a list of patches to turn text1 into text2. text2 is ignored,
	 * diffs are the delta between text1 and text2.
	 * 
	 * @param text1 Old text
	 * @param text2 Ignored.
	 * @param diffs
	 *            Array of diff tuples for text1 to text2.
	 * @return LinkedList of Patch objects.
	 * @deprecated Prefer patch_make(String text1, LinkedList<Diff> diffs).
	 */
	public List<Patch> compute(String text1, String text2, List<Diff> diffs) {
		return compute(text1, diffs);
	}

	/**
	 * Compute a list of patches to turn text1 into text2. text2 is not
	 * provided, diffs are the delta between text1 and text2.
	 * 
	 * @param baseSource
	 *            Old text.
	 * @param diffs
	 *            Array of diff tuples for text1 to text2.
	 * @return LinkedList of Patch objects.
	 */
	public static List<Patch> compute(String baseSource, List<Diff> diffs) {
		
		LinkedList<Patch> patches = new LinkedList<Patch>();
		if (diffs.isEmpty()) {
			return patches; // Get rid of the null case.
		}
		Patch patch = new Patch();
		int charCount1 = 0; // Number of characters into the text1 string.
		int charCount2 = 0; // Number of characters into the text2 string.
		// Start with text1 (prepatch_text) and apply the diffs until we arrive
		// at
		// text2 (postpatch_text). We recreate the patches one by one to
		// determine
		// context info.
		String prepatchText = baseSource;
		String postpatchText = baseSource;
		for (Diff diff : diffs) {
			if (patch.diffs.isEmpty() && diff.operation != Operation.EQUAL) {
				// A new patch starts here.
				patch.start1 = charCount1;
				patch.start2 = charCount2;
			}

			switch (diff.operation) {
			case INSERT:
				patch.diffs.add(diff);
				patch.length2 += diff.content.length();
				postpatchText = postpatchText.substring(0, charCount2) + diff.content
						+ postpatchText.substring(charCount2);
				break;
			case DELETE:
				patch.length1 += diff.content.length();
				patch.diffs.add(diff);
				postpatchText = postpatchText.substring(0, charCount2)
						+ postpatchText.substring(charCount2 + diff.content.length());
				break;
			case EQUAL:
				if (diff.content.length() <= 2 * MARGIN && !patch.diffs.isEmpty() && diff != diffs.get(diffs.size()-1)) {
					// Small equality inside a patch.
					patch.diffs.add(diff);
					patch.length1 += diff.content.length();
					patch.length2 += diff.content.length();
				}

				if (diff.content.length() >= 2 * MARGIN) {
					// Time for a new patch.
					if (!patch.diffs.isEmpty()) {
						addContext(patch, prepatchText);
						patches.add(patch);
						patch = new Patch();
						// Unlike Unidiff, our patch lists have a rolling context.
						// http://code.google.com/p/google-diff-match-patch/wiki/Unidiff
						// Update prepatch text & pos to reflect the application of the just completed patch.
						prepatchText = postpatchText;
						charCount1 = charCount2;
					}
				}
				break;
			}

			// Update the current character count.
			if (diff.operation != Operation.INSERT) {
				charCount1 += diff.content.length();
			}
			if (diff.operation != Operation.DELETE) {
				charCount2 += diff.content.length();
			}
		}
		// Pick up the leftover patch if not empty.
		if (!patch.diffs.isEmpty()) {
			addContext(patch, prepatchText);
			patches.add(patch);
		}

		return patches;
	}

	/**
	 * Given an array of patches, return another array that is identical.
	 * 
	 * @param patches Array of patch objects.
	 * @return Array of patch objects.
	 */
	public static List<Patch> deepCopy(List<Patch> patches) {
		
		List<Patch> patchesCopy = new LinkedList<Patch>();
		
		for (Patch aPatch : patches) {
			Patch patchCopy = new Patch();
			for (Diff diff : aPatch.diffs) {
				Diff diffCopy = new Diff(diff.operation, diff.content);
				patchCopy.diffs.add(diffCopy);
			}
			patchCopy.start1 = aPatch.start1;
			patchCopy.start2 = aPatch.start2;
			patchCopy.length1 = aPatch.length1;
			patchCopy.length2 = aPatch.length2;
			patchesCopy.add(patchCopy);
		}
		return patchesCopy;
	}

	/**
	 * Merge a set of patches onto the text. Return a patched text, as well as
	 * an array of true/false values indicating which patches were applied.
	 * 
	 * @param patches Array of patch objects
	 * @param text Old text.
	 * @return Two element Object array, containing the new text and an array of
	 *         boolean values.
	 */
	public static Object[] apply(List<Patch> patches, String text) {
		if (patches.isEmpty()) {
			return new Object[] { text, new boolean[0] };
		}

		// Deep copy the patches so that no changes are made to originals.
		patches = deepCopy(patches);

		String nullPadding = addPadding(patches);
		text = nullPadding + text + nullPadding;
		splitMax(patches);

		int x = 0;
		// delta keeps track of the offset between the expected and actual location
		// of the previous patch. If there are patches expected at positions 10 and
		// 20, but the first patch was found at 12, delta is 2 and the second patch
		// has an effective expected position of 22. 
		int delta = 0;
		boolean[] results = new boolean[patches.size()];
		
		for (Patch patch : patches) {
			int expectedLoc = patch.start2 + delta;
			String sourceText = Diff.computeSource(patch.diffs);
			int startLoc = 0;
			int endLoc = -1;
			if (sourceText.length() > Match.MAX_BITS) {
				// patch_splitMax will only provide an oversized pattern in the
				// case of
				// a monster delete.
				startLoc = Match.find(text, sourceText.substring(0, Match.MAX_BITS), expectedLoc);
				if (startLoc != -1) {
					endLoc = Match.find(text, sourceText.substring(sourceText.length() - Match.MAX_BITS),
							expectedLoc + sourceText.length() - Match.MAX_BITS);
					if (endLoc == -1 || startLoc >= endLoc) {
						// Can't find valid trailing context. Drop this patch.
						startLoc = -1;
					}
				}
			} else {
				startLoc = Match.find(text, sourceText, expectedLoc);
			}
			if (startLoc == -1) {
				// No match found. :(
				results[x] = false;
				// Subtract the delta for this failed patch from subsequent
				// patches.
				delta -= patch.length2 - patch.length1;
			} else {
				// Found a match. :)
				results[x] = true;
				delta = startLoc - expectedLoc;
				String matchText = "";
				if (endLoc == -1) {
					matchText = text.substring(startLoc, Math.min(startLoc + sourceText.length(), text.length()));
				} else {
					matchText = text.substring(startLoc, Math.min(endLoc + Match.MAX_BITS, text.length()));
				}
				if (sourceText.equals(matchText)) {
					// Perfect match, just shove the replacement text in.
					text = text.substring(0, startLoc) + Diff.computeDestination(patch.diffs)
							+ text.substring(startLoc + sourceText.length());
				} else {
					// Imperfect match. Run a diff to get a framework of equivalent indices.
					List<Diff> diffs = Diff.find(sourceText, matchText, false);
					if (sourceText.length() > Match.MAX_BITS
							&& Diff.levenshtein(diffs) / (float) sourceText.length() > DELETE_THRESHOLD) {
						// The end points match, but the content is unacceptably bad.
						results[x] = false;
					} else {
						Diff.cleanupSemanticLossless(diffs);
						int index = 0;
						for (Diff diff : patch.diffs) {
							if (diff.operation != Operation.EQUAL) {
								int lastIndex = Diff.xIndex(diffs, index);
								if (diff.operation == Operation.INSERT) {
									// Insertion
									text = text.substring(0, startLoc + lastIndex) + diff.content
											+ text.substring(startLoc + lastIndex);
								} else if (diff.operation == Operation.DELETE) {
									// Deletion
									text = text.substring(0, startLoc + lastIndex) + text
											.substring(startLoc + Diff.xIndex(diffs, index + diff.content.length()));
								}
							}
							if (diff.operation != Operation.DELETE) {
								index += diff.content.length();
							}
						}
					}
				}
			}
			x++;
		}
		// Strip the padding off.
		text = text.substring(nullPadding.length(), text.length() - nullPadding.length());
		return new Object[] { text, results };
	}

	/**
	 * Add some padding on text start and end so that edges can match something.
	 * Intended to be called only from within patch_apply.
	 * 
	 * @param patches
	 *            Array of patch objects.
	 * @return The padding string added to each side.
	 */
	public static String addPadding(List<Patch> patches) {
		int paddingLength = MARGIN;
		String nullPadding = "";
		for (int x = 1; x <= paddingLength; x++) {
			nullPadding += String.valueOf((char) x);
		}

		// Bump all the patches forward.
		for (Patch patch : patches) {
			patch.start1 += paddingLength;
			patch.start2 += paddingLength;
		}

		// Add some padding on start of first diff.
		Patch patch = patches.get(0);
		LinkedList<Diff> diffs = patch.diffs;
		
		if (diffs.isEmpty() || diffs.getFirst().operation != Operation.EQUAL) {
			// Add nullPadding equality.
			diffs.addFirst(new Diff(Operation.EQUAL, nullPadding));
			patch.start1 -= paddingLength; // Should be 0.
			patch.start2 -= paddingLength; // Should be 0.
			patch.length1 += paddingLength;
			patch.length2 += paddingLength;
		} else if (paddingLength > diffs.getFirst().content.length()) {
			// Grow first equality.
			Diff firstDiff = diffs.getFirst();
			int extraLength = paddingLength - firstDiff.content.length();
			firstDiff.content = nullPadding.substring(firstDiff.content.length()) + firstDiff.content;
			patch.start1 -= extraLength;
			patch.start2 -= extraLength;
			patch.length1 += extraLength;
			patch.length2 += extraLength;
		}

		// Add some padding on end of last diff.
		patch = patches.get(patches.size()-1);
		diffs = patch.diffs;
		if (diffs.isEmpty() || diffs.getLast().operation != Operation.EQUAL) {
			// Add nullPadding equality.
			diffs.addLast(new Diff(Operation.EQUAL, nullPadding));
			patch.length1 += paddingLength;
			patch.length2 += paddingLength;
		} else if (paddingLength > diffs.getLast().content.length()) {
			// Grow last equality.
			Diff lastDiff = diffs.getLast();
			int extraLength = paddingLength - lastDiff.content.length();
			lastDiff.content += nullPadding.substring(0, extraLength);
			patch.length1 += extraLength;
			patch.length2 += extraLength;
		}

		return nullPadding;
	}

	/**
	 * Look through the patches and break up any which are longer than the
	 * maximum limit of the match algorithm.
	 * 
	 * @param patches LinkedList of Patch objects.
	 */
	public static void splitMax(List<Patch> patches) {
		
		int patchSize = Match.MAX_BITS;
		String preContext = "";
		String postContext = "";
		Patch patch = null;
		int start1 = 0, start2 = 0;
		boolean empty = true;
		Operation diffType;
		String diffText = "";
		
		ListIterator<Patch> pointer = patches.listIterator();
		Patch bigpatch = pointer.hasNext() ? pointer.next() : null;
		while (bigpatch != null) {
			if (bigpatch.length1 <= Match.MAX_BITS) {
				bigpatch = pointer.hasNext() ? pointer.next() : null;
				continue;
			}
			// Remove the big old patch.
			pointer.remove();
			start1 = bigpatch.start1;
			start2 = bigpatch.start2;
			preContext = "";
			while (!bigpatch.diffs.isEmpty()) {
				// Create one of several smaller patches.
				patch = new Patch();
				empty = true;
				patch.start1 = start1 - preContext.length();
				patch.start2 = start2 - preContext.length();
				if (preContext.length() != 0) {
					patch.length1 = patch.length2 = preContext.length();
					patch.diffs.add(new Diff(Operation.EQUAL, preContext));
				}
				while (!bigpatch.diffs.isEmpty() && patch.length1 < patchSize - MARGIN) {
					diffType = bigpatch.diffs.getFirst().operation;
					diffText = bigpatch.diffs.getFirst().content;
					if (diffType == Operation.INSERT) {
						// Insertions are harmless.
						patch.length2 += diffText.length();
						start2 += diffText.length();
						patch.diffs.addLast(bigpatch.diffs.removeFirst());
						empty = false;
					} else if (diffType == Operation.DELETE && patch.diffs.size() == 1
							&& patch.diffs.getFirst().operation == Operation.EQUAL
							&& diffText.length() > 2 * patchSize) {
						// This is a large deletion. Let it pass in one chunk.
						patch.length1 += diffText.length();
						start1 += diffText.length();
						empty = false;
						patch.diffs.add(new Diff(diffType, diffText));
						bigpatch.diffs.removeFirst();
					} else {
						// Deletion or equality. Only take as much as we can
						// stomach.
						diffText = diffText.substring(0,
								Math.min(diffText.length(), patchSize - patch.length1 - MARGIN));
						patch.length1 += diffText.length();
						start1 += diffText.length();
						if (diffType == Operation.EQUAL) {
							patch.length2 += diffText.length();
							start2 += diffText.length();
						} else {
							empty = false;
						}
						patch.diffs.add(new Diff(diffType, diffText));
						if (diffText.equals(bigpatch.diffs.getFirst().content)) {
							bigpatch.diffs.removeFirst();
						} else {
							bigpatch.diffs.getFirst().content = bigpatch.diffs.getFirst().content
									.substring(diffText.length());
						}
					}
				}
				// Compute the head context for the next patch.
				preContext = Diff.computeDestination(patch.diffs);
				preContext = preContext.substring(Math.max(0, preContext.length() - MARGIN));
				// Append the end context for this patch.
				if (Diff.computeSource(bigpatch.diffs).length() > MARGIN) {
					postContext = Diff.computeSource(bigpatch.diffs).substring(0, MARGIN);
				} else {
					postContext = Diff.computeSource(bigpatch.diffs);
				}
				if (postContext.length() != 0) {
					patch.length1 += postContext.length();
					patch.length2 += postContext.length();
					if (!patch.diffs.isEmpty() && patch.diffs.getLast().operation == Operation.EQUAL) {
						patch.diffs.getLast().content += postContext;
					} else {
						patch.diffs.add(new Diff(Operation.EQUAL, postContext));
					}
				}
				if (!empty) {
					pointer.add(patch);
				}
			}
			bigpatch = pointer.hasNext() ? pointer.next() : null;
		}
	}

	/**
	 * Take a list of patches and return a textual representation.
	 * 
	 * @param patches List of Patch objects.
	 * @return Text representation of patches.
	 */
	public static String toString(List<Patch> patches) {
		StringBuilder text = new StringBuilder();
		for (Patch aPatch : patches) {
			text.append(aPatch);
		}
		return text.toString();
	}

	/**
	 * Parse a textual representation of patches and return a List of Patch
	 * objects.
	 * 
	 * @param textline Text representation of patches.
	 * @return List of Patch objects.
	 * @throws IllegalArgumentException if invalid input.
	 */
	public static List<Patch> fromString(String textline) throws IllegalArgumentException {
		
		List<Patch> patches = new LinkedList<Patch>();
		if (textline.length() == 0) {
			return patches;
		}
		
		List<String> textList = Arrays.asList(textline.split("\n"));
		LinkedList<String> text = new LinkedList<String>(textList);
		
		Patch patch = null;
		
		while (!text.isEmpty()) {
			Matcher m = PATCH_HEADER.matcher(text.getFirst());
			if (!m.matches()) {
				throw new IllegalArgumentException("Invalid patch string: " + text.getFirst());
			}
			patch = new Patch();
			patches.add(patch);
			patch.start1 = Integer.parseInt(m.group(1));
			if (m.group(2).length() == 0) {
				patch.start1--;
				patch.length1 = 1;
			} else if (m.group(2).equals("0")) {
				patch.length1 = 0;
			} else {
				patch.start1--;
				patch.length1 = Integer.parseInt(m.group(2));
			}

			patch.start2 = Integer.parseInt(m.group(3));
			if (m.group(4).length() == 0) {
				patch.start2--;
				patch.length2 = 1;
			} else if (m.group(4).equals("0")) {
				patch.length2 = 0;
			} else {
				patch.start2--;
				patch.length2 = Integer.parseInt(m.group(4));
			}
			text.removeFirst();

			while (!text.isEmpty()) {
				char sign = 0;
				try {
					sign = text.getFirst().charAt(0);
				} catch (IndexOutOfBoundsException e) {
					// Blank line? Whatever.
					text.removeFirst();
					continue;
				}
				String line = text.getFirst().substring(1);
				line = line.replace("+", "%2B"); // decode would change all "+"
													// to " "
				try {
					line = URLDecoder.decode(line, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// Not likely on modern system.
					throw new Error("This system does not support UTF-8.", e);
				} catch (IllegalArgumentException e) {
					// Malformed URI sequence.
					throw new IllegalArgumentException("Illegal escape in patch_fromText: " + line, e);
				}
				if (sign == '-') {
					// Deletion.
					patch.diffs.add(new Diff(Operation.DELETE, line));
				} else if (sign == '+') {
					// Insertion.
					patch.diffs.add(new Diff(Operation.INSERT, line));
				} else if (sign == ' ') {
					// Minor equality.
					patch.diffs.add(new Diff(Operation.EQUAL, line));
				} else if (sign == '@') {
					// Start of next patch.
					break;
				} else {
					// WTF?
					throw new IllegalArgumentException("Invalid patch mode '" + sign + "' in: " + line);
				}
				text.removeFirst();
			}
		}
		return patches;
	}
	
	/**
	 * Increase the context until it is unique, but don't let the pattern expand
	 * beyond Match_MaxBits.
	 * 
	 * @param patch The patch to grow.
	 * @param textSource text.
	 */
	protected static void addContext(Patch patch, String text) {
		if (text.length() == 0) {
			return;
		}
		String pattern = text.substring(patch.start2, patch.start2 + patch.length1);
		int padding = 0;

		// Look for the first and last matches of pattern in text. If two different
		// matches are found, increase the pattern length.
		while (text.indexOf(pattern) != text.lastIndexOf(pattern)
				&& pattern.length() < Match.MAX_BITS - MARGIN - MARGIN) {
			padding += MARGIN;
			pattern = text.substring(Math.max(0, patch.start2 - padding),
					Math.min(text.length(), patch.start2 + patch.length1 + padding));
		}
		// Add one chunk for good luck.
		padding += MARGIN;

		// Add the prefix.
		String prefix = text.substring(Math.max(0, patch.start2 - padding), patch.start2);
		if (prefix.length() != 0) {
			patch.diffs.addFirst(new Diff(Operation.EQUAL, prefix));
		}
		// Add the suffix.
		String suffix = text.substring(patch.start2 + patch.length1,
				Math.min(text.length(), patch.start2 + patch.length1 + padding));
		if (suffix.length() != 0) {
			patch.diffs.addLast(new Diff(Operation.EQUAL, suffix));
		}

		// Roll back the start points.
		patch.start1 -= prefix.length();
		patch.start2 -= prefix.length();
		// Extend the lengths.
		patch.length1 += prefix.length() + suffix.length();
		patch.length2 += prefix.length() + suffix.length();
	}
}
