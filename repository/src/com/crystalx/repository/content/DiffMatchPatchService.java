package com.crystalx.repository.content;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.log4j.Logger;

import com.crystalx.repository.common.Source;
import com.crystalx.repository.common.SourceManageService;
import com.crystalx.repository.common.StringSource;

public class DiffMatchPatchService implements SourceManageService {

	private static final Logger logger = Logger.getLogger(DiffMatchPatchService.class);
	
	@Override
	public String calculateDifference(Source previous, Source current) {
		List<Patch> patches = Patch.compute(previous.retreiveAllSource(), current.retreiveAllSource());
		return  Patch.toString(patches);
	}

	@Override
	public void calculateDifference(Source previous, Source current, OutputStream out) {
		List<Patch> patches = Patch.compute(previous.retreiveAllSource(), current.retreiveAllSource());
		String patch = Patch.toString(patches);
		
		try {
			out.write(patch.getBytes());
		} catch (IOException e) {
			logger.error( "Failed to write patch to stream: ",e);
		}
		
	}

	@Override
	public String applyPatch(Source source, Source patch) {
		List<Patch> restoredPatches = Patch.fromString(patch.retreiveAllSource());
		return (String) Patch.apply(restoredPatches, source.retreiveAllSource())[0];
	}

	@Override
	public void applyPatch(Source source, Source patch, OutputStream out) {
		List<Patch> restoredPatches = Patch.fromString(patch.retreiveAllSource());
		String restored = (String) Patch.apply(restoredPatches, source.retreiveAllSource())[0];
		
		try {
			out.write(restored.getBytes());
		} catch (IOException e) {
			logger.error("Failed to write restored source version to stream: ", e);
		}
		
	}

	@Override
	public String applyPatches(Source origin, List<Source> patches) {
		String current = origin.retreiveAllSource();
		for(Source p: patches) {
			current = applyPatch(new StringSource(current), p);
		}
		return current;
	}

	@Override
	public void applyPatches(Source origin, List<Source> patches, OutputStream out) {
		String current = origin.retreiveAllSource();
		for(Source p: patches) {
			current = applyPatch(new StringSource(current), p);
		}

		try {
			out.write(current.getBytes());
		} catch (IOException e) {
			logger.error("Failed to write restored version(from patch chains) to stream: ", e);
		}
		
	}

}
