package com.crystalx.repository.content;

public class CommonUtils {

	/**
	 * Compute a good hash of two integers.
	 * 
	 * @param x First int.
	 * @param ySecond int.
	 * @return A long made up of both ints.
	 */
	public static long footprint(int x, int y) {
		// The maximum size for a long is 9,223,372,036,854,775,807
		// The maximum size for an int is 2,147,483,647
		// Two ints fit nicely in one long.
		long result = x;
		result = result << 32;
		result += y;
		return result;
	}
}
