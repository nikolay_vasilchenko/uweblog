package com.crystalx.repository.content;

import java.util.HashMap;
import java.util.Map;

public class Match {

	/**
	 * At what point is no match declared (0.0 = perfection, 1.0 = very loose).
	 */
	public static final float THRESHOLD = 0.5f;
	/**
	 * How far to search for a match (0 = exact location, 1000+ = broad match).
	 * A match this many characters away from the expected location will add 1.0
	 * to the score (0.0 is a perfect match).
	 */
	public static final int DISTANCE = 1000;

	/**
	 * The number of bits in an int.
	 */
	static final int MAX_BITS = 32;

	/**
	 * Locate the best instance of 'pattern' in 'text' near 'loc'. Returns -1 if no match found.
	 * 
	 * @param text The text to search.
	 * @param pattern The pattern to search for.
	 * @param loc The location to search around.
	 * @return Best match index or -1.
	 */
	public static int find(String text, String pattern, int loc) {
		
		loc = Math.max(0, Math.min(loc, text.length()));
		if (text.equals(pattern)) {
			// Shortcut (potentially not guaranteed by the algorithm)
			return 0;
		} else if (text.length() == 0) {
			// Nothing to match.
			return -1;
		} else if (loc + pattern.length() <= text.length()
				&& text.substring(loc, loc + pattern.length()).equals(pattern)) {
			// Perfect match at the perfect spot! (Includes case of null
			// pattern)
			return loc;
		} else {
			// Do a fuzzy compare.
			return bitap(text, pattern, loc);
		}
	}

	/**
	 * Locate the best instance of 'pattern' in 'text' near 'loc' using the
	 * Bitap algorithm. Returns -1 if no match found.
	 * 
	 * @param text The text to search.
	 * @param pattern The pattern to search for.
	 * @param loc The location to search around.
	 * @return Best match index or -1.
	 */
	protected static int bitap(String text, String pattern, int loc) {
		assert (MAX_BITS == 0 || pattern.length() <= MAX_BITS) : "Pattern too long for this application.";

		// Initialize the alphabet.
		Map<Character, Integer> s = alphabet(pattern);

		// Highest score beyond which we give up.
		double scoreThreshold = THRESHOLD;
		// Is there a nearby exact match? (speedup)
		int bestLoc = text.indexOf(pattern, loc);
		if (bestLoc != -1) {
			scoreThreshold = Math.min(bitapScore(0, bestLoc, loc, pattern), scoreThreshold);
			// What about in the other direction? (speedup)
			bestLoc = text.lastIndexOf(pattern, loc + pattern.length());
			if (bestLoc != -1) {
				scoreThreshold = Math.min(bitapScore(0, bestLoc, loc, pattern), scoreThreshold);
			}
		}

		// Initialize the bit arrays.
		int matchmask = 1 << (pattern.length() - 1);
		bestLoc = -1;

		int binMin = 0;
		int binMid = 0;
		int binMax = pattern.length() + text.length();
		// Empty initialization added to appease Java compiler.
		int[] lastRd = new int[0];
		for (int d = 0; d < pattern.length(); d++) {
			// Scan for the best match; each iteration allows for one more error.
			// Run a binary search to determine how far from 'loc' we can stray at this error level.
			binMin = 0;
			binMid = binMax;
			while (binMin < binMid) {
				if (bitapScore(d, loc + binMid, loc, pattern) <= scoreThreshold) {
					binMin = binMid;
				} else {
					binMax = binMid;
				}
				binMid = (binMax - binMin) / 2 + binMin;
			}
			// Use the result from this iteration as the maximum for the next.
			binMax = binMid;
			int start = Math.max(1, loc - binMid + 1);
			int finish = Math.min(loc + binMid, text.length()) + pattern.length();

			int[] rd = new int[finish + 2];
			rd[finish + 1] = (1 << d) - 1;
			for (int j = finish; j >= start; j--) {
				int charMatch;
				if (text.length() <= j - 1 || !s.containsKey(text.charAt(j - 1))) {
					// Out of range.
					charMatch = 0;
				} else {
					charMatch = s.get(text.charAt(j - 1));
				}
				if (d == 0) {
					// First pass: exact match.
					rd[j] = ((rd[j + 1] << 1) | 1) & charMatch;
				} else {
					// Subsequent passes: fuzzy match.
					rd[j] = ((rd[j + 1] << 1) | 1) & charMatch | (((lastRd[j + 1] | lastRd[j]) << 1) | 1)
							| lastRd[j + 1];
				}
				if ((rd[j] & matchmask) != 0) {
					double score = bitapScore(d, j - 1, loc, pattern);
					// This match will almost certainly be better than any existing
					// match. But check anyway.
					if (score <= scoreThreshold) {
						// Told you so.
						scoreThreshold = score;
						bestLoc = j - 1;
						if (bestLoc > loc) {
							// When passing loc, don't exceed our current distance from loc.
							start = Math.max(1, 2 * loc - bestLoc);
						} else {
							// Already passed loc, downhill from here on in.
							break;
						}
					}
				}
			}
			if (bitapScore(d + 1, loc, loc, pattern) > scoreThreshold) {
				// No hope for a (better) match at greater error levels.
				break;
			}
			lastRd = rd;
		}
		return bestLoc;
	}

	/**
	 * Compute and return the score for a match with e errors and x location.
	 * 
	 * @param e Number of errors in match.
	 * @param x Location of match.
	 * @param loc Expected location of match.
	 * @param pattern
	 *            Pattern being sought.
	 * @return Overall score for match (0.0 = good, 1.0 = bad).
	 */
	private static double bitapScore(int e, int x, int loc, String pattern) {
		float accuracy = (float) e / pattern.length();
		int proximity = Math.abs(loc - x);
		return accuracy + (proximity / (float) DISTANCE);
	}

	/**
	 * Initialize the alphabet for the Bitap algorithm.
	 * 
	 * @param pattern The text to encode.
	 * @return Hash of character locations.
	 */
	protected static Map<Character, Integer> alphabet(String pattern) {
		Map<Character, Integer> result = new HashMap<Character, Integer>();
		char[] charPattern = pattern.toCharArray();
		for (char ch : charPattern) {
			result.put(ch, 0);
		}
		int i = 0;
		for (char ch : charPattern) {
			result.put(ch, result.get(ch) | (1 << (pattern.length() - i - 1)));
			i++;
		}
		return result;
	}

}
