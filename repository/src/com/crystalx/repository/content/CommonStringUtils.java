package com.crystalx.repository.content;

public class CommonStringUtils {

	/**
	 * Determine the common prefix of two strings
	 * 
	 * @param first First string.
	 * @param second Second string.
	 * @return The number of characters common to the start of each string.
	 */
	public static int computeCommonPrefix(String first, String second) {

		int n = Math.min(first.length(), second.length());
		for (int i = 0; i < n; i++) {
			if (first.charAt(i) != second.charAt(i)) {
				return i;
			}
		}
		return n;
	}

	/**
	 * Determine the common suffix of two strings
	 * 
	 * @param first First string.
	 * @param second Second string.
	 * @return The number of characters common to the end of each string.
	 */
	public static int computeCommonSuffix(String first, String second) {

		int firstLength = first.length();
		int SecondLength = second.length();
		int len = Math.min(firstLength, SecondLength);
		for (int i = 1; i <= len; i++) {
			if (first.charAt(firstLength - i) != second.charAt(SecondLength - i)) {
				return i - 1;
			}
		}
		return len;
	}
	
	/**
	 * Unescape selected chars for compatability with JavaScript's encodeURI. In
	 * speed critical applications this could be dropped since the receiving
	 * application will certainly decode these fine. Note that this function is
	 * case-sensitive. Thus "%3f" would not be unescaped. But this is ok because
	 * it is only called with the output of URLEncoder.encode which returns
	 * uppercase hex.
	 *
	 * Example: "%3F" -> "?", "%24" -> "$", etc.
	 *
	 * @param strThe string to escape.
	 * @return The escaped string.
	 */
	public static String unescapeForEncodeUriCompatability(String str) {
		return str.replace("%21", "!").replace("%7E", "~").replace("%27", "'").replace("%28", "(").replace("%29", ")")
				.replace("%3B", ";").replace("%2F", "/").replace("%3F", "?").replace("%3A", ":").replace("%40", "@")
				.replace("%26", "&").replace("%3D", "=").replace("%2B", "+").replace("%24", "$").replace("%2C", ",")
				.replace("%23", "#");
	}

}
