package com.crystalx.repository.content;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class PathUtils {

	/**
	 * Work from the middle back to the start to determine the path.
	 * 
	 * @param v_map List of path sets.
	 * @param text1 Old string fragment to be diffed.
	 * @param text2 New string fragment to be diffed.
	 * @return LinkedList of Diff objects.
	 */
	protected static LinkedList<Diff> middleBackwardPath(List<Set<Long>> v_map, String text1, String text2) {
		LinkedList<Diff> path = new LinkedList<Diff>();
		int x = text1.length();
		int y = text2.length();
		Operation last_op = null;
		for (int d = v_map.size() - 2; d >= 0; d--) {
			while (true) {
				if (v_map.get(d).contains(CommonUtils.footprint(x - 1, y))) {
					x--;
					if (last_op == Operation.DELETE) {
						path.getFirst().content = text1.charAt(x) + path.getFirst().content;
					} else {
						path.addFirst(new Diff(Operation.DELETE, text1.substring(x, x + 1)));
					}
					last_op = Operation.DELETE;
					break;
				} else if (v_map.get(d).contains(CommonUtils.footprint(x, y - 1))) {
					y--;
					if (last_op == Operation.INSERT) {
						path.getFirst().content = text2.charAt(y) + path.getFirst().content;
					} else {
						path.addFirst(new Diff(Operation.INSERT, text2.substring(y, y + 1)));
					}
					last_op = Operation.INSERT;
					break;
				} else {
					x--;
					y--;
					assert (text1.charAt(x) == text2.charAt(y)) : "No diagonal.  Can't happen. (diff_path1)";
					if (last_op == Operation.EQUAL) {
						path.getFirst().content = text1.charAt(x) + path.getFirst().content;
					} else {
						path.addFirst(new Diff(Operation.EQUAL, text1.substring(x, x + 1)));
					}
					last_op = Operation.EQUAL;
				}
			}
		}
		return path;
	}

	/**
	 * Work from the middle back to the end to determine the path.
	 * 
	 * @param v_map List of path sets.
	 * @param text1 Old string fragment to be diffed.
	 * @param text2 New string fragment to be diffed.
	 * @return LinkedList of Diff objects.
	 */
	protected static LinkedList<Diff> middleForvardPath(List<Set<Long>> v_map, String text1, String text2) {
		
		LinkedList<Diff> path = new LinkedList<Diff>();
		int x = text1.length();
		int y = text2.length();
		Operation last_op = null;
		for (int d = v_map.size() - 2; d >= 0; d--) {
			while (true) {
				if (v_map.get(d).contains(CommonUtils.footprint(x - 1, y))) {
					x--;
					if (last_op == Operation.DELETE) {
						path.getLast().content += text1.charAt(text1.length() - x - 1);
					} else {
						path.addLast(new Diff(Operation.DELETE,
								text1.substring(text1.length() - x - 1, text1.length() - x)));
					}
					last_op = Operation.DELETE;
					break;
				} else if (v_map.get(d).contains(CommonUtils.footprint(x, y - 1))) {
					y--;
					if (last_op == Operation.INSERT) {
						path.getLast().content += text2.charAt(text2.length() - y - 1);
					} else {
						path.addLast(new Diff(Operation.INSERT,
								text2.substring(text2.length() - y - 1, text2.length() - y)));
					}
					last_op = Operation.INSERT;
					break;
				} else {
					x--;
					y--;
					assert (text1.charAt(text1.length() - x - 1) == text2
							.charAt(text2.length() - y - 1)) : "No diagonal.  Can't happen. (diff_path2)";
					if (last_op == Operation.EQUAL) {
						path.getLast().content += text1.charAt(text1.length() - x - 1);
					} else {
						path.addLast(
								new Diff(Operation.EQUAL, text1.substring(text1.length() - x - 1, text1.length() - x)));
					}
					last_op = Operation.EQUAL;
				}
			}
		}
		return path;
	}

}
