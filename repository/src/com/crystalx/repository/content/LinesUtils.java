package com.crystalx.repository.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LinesUtils {
	
	private static final char ENDL = '\n';

	/**
	 * Split two texts into a list of strings. Reduce the texts to a string of
	 * hashes where each Unicode character represents one line.
	 * 
	 * @param first First string.
	 * @param second Second string.
	 * @return An object containing the encoded text1, the encoded text2 and the
	 *         List of unique strings. The zeroth element of the List of unique
	 *         strings is intentionally blank.
	 */
	public static LinesToCharsResult linesToChars(String first, String second) {
		List<String> lineArray = new ArrayList<String>();
		Map<String, Integer> lineHash = new HashMap<String, Integer>();
		// e.g. linearray[4] == "Hello\n"
		// e.g. linehash.get("Hello\n") == 4

		// "\x00" is a valid character, but various debuggers don't like it.
		// So we'll insert a junk entry to avoid generating a null character.
		lineArray.add("");

		String firstChars = linesToCharsMunge(first, lineArray, lineHash);
		String secondChars = linesToCharsMunge(second, lineArray, lineHash);
		
		return new LinesToCharsResult(firstChars, secondChars, lineArray);
	}
	
	/**
	 * Rehydrate the text in a diff from a string of line hashes to real lines of text.
	 * 
	 * @param diffs LinkedList of Diff objects.
	 * @param lineArray List of unique strings.
	 */
	protected static void charsToLines(List<Diff> diffs, List<String> lineArray) {
		StringBuilder text;
		for (Diff diff : diffs) {
			text = new StringBuilder();
			String content = diff.getContent();
			for (int y = 0; y < content.length(); y++) {
				text.append(lineArray.get(content.charAt(y)));
			}
			diff.updateContent(text.toString());
		}
	}
	
	/**
	 * Split a text into a list of strings. Reduce the texts to a string of
	 * hashes where each Unicode character represents one line.
	 * 
	 * @param text String to encode.
	 * @param lineArray List of unique strings.
	 * @param lineHash Map of strings to indices.
	 * @return Encoded string.
	 */
	private static String linesToCharsMunge(String text, List<String> lineArray, Map<String, Integer> lineHash) {
		int lineStart = 0;
		int lineEnd = -1;
		String line = null;
		StringBuilder chars = new StringBuilder();
		
		// Walk the text, pulling out a substring for each line.
		// text.split('\n') would would temporarily double our memory footprint.
		// Modifying text would create many large strings to garbage collect.
		
		while (lineEnd < text.length() - 1) {
			lineEnd = text.indexOf(ENDL, lineStart);
			if (lineEnd == -1) {
				lineEnd = text.length() - 1;
			}
			line = text.substring(lineStart, lineEnd + 1);
			lineStart = lineEnd + 1;

			if (lineHash.containsKey(line)) {
				chars.append(String.valueOf((char) (int) lineHash.get(line)));
			} else {
				lineArray.add(line);
				lineHash.put(line, lineArray.size() - 1);
				chars.append(String.valueOf((char) (lineArray.size() - 1)));
			}
		}
		return chars.toString();
	}
}
