package com.crystalx.repository.exception;

public class NoSuchObjectException extends Exception {

	private static final long serialVersionUID = 4088512878897043613L;

	public NoSuchObjectException() {
		super();
	}

	public NoSuchObjectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public NoSuchObjectException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoSuchObjectException(String message) {
		super(message);
	}

	public NoSuchObjectException(Throwable cause) {
		super(cause);
	}
	
}
