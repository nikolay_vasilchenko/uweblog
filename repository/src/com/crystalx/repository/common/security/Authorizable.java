package com.crystalx.repository.common.security;

public interface Authorizable {

	long getSID();
	String getLoginName();
	boolean isActive();
	
}
