package com.crystalx.repository.common.security;

public enum SecurityPrivileges {

	/**
	 * No access rights at all
	 */
	NONE(100),
	
	/**
	 * Allows only data fetching
	 */
	FETCH(101),
	
	/**
	 * Allows to modify already existing objects (fetch included)
	 */
	MODIFY(102),
	
	/**
	 * Allows create (fetch and modify are included)
	 */
	CREATE(103),
	
	/**
	 * Allows all operations(fetch, modify, create, grant access to another user)
	 */
	SUPER(110);
	
	private int code;
	
	SecurityPrivileges(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
	
	public static SecurityPrivileges fromString(String data) {
		
		try {
			int code = Integer.parseInt(data);
			
			for(SecurityPrivileges r: values()) {
				if(r.code == code){
					return r;
				}
			}
		}catch (NumberFormatException e) {
			// not a number
			return valueOf(data);
		}
		
		return null;
	}
	
	@Override
	public String toString() {
		return super.toString() + "[" + Integer.toString(code) + "]";
	}
}
