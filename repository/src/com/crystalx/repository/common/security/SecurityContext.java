package com.crystalx.repository.common.security;

import com.crystalx.repository.common.Document;
import com.crystalx.repository.exception.SecurityException;

public interface SecurityContext {

	Authorizable findUser(long sid);
	
	SecurityPrivileges fetchUserGeneralPrivileges(Authorizable user);
	
	SecurityPrivileges fetchUserPrivileges(Authorizable user, Document document);
	
	SecurityPrivileges fetchUserPrivileges(Authorizable user, String module);
	
	void setGeneralRepositoryPrivileges(Authorizable initiator, Authorizable target,
			SecurityPrivileges sp) throws SecurityException;
	
	
	void setPrivileges(Authorizable initiator, Authorizable target,
			Document document, SecurityPrivileges sp) throws SecurityException;
	
	void setPrivileges(Authorizable initiator, Authorizable target,
			String module, SecurityPrivileges sp) throws SecurityException;
	
	void setGroupPrivileges(Authorizable initiator, Authorizable target,
			Document document, SecurityPrivileges sp) throws SecurityException;
	
	//Authorizable createNewUser(Authorizable initiator, String loginName, String password) throws SecurityException;
}
