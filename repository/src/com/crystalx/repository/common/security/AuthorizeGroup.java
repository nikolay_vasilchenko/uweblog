package com.crystalx.repository.common.security;

import java.util.List;

public interface AuthorizeGroup {

	long getSID();
	boolean isActive();
	String getLongName();
	boolean isMemeber(Authorizable authorizable);
	List<Authorizable> listAllMemebers();
}
