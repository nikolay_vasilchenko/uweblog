package com.crystalx.repository.common;

import java.io.Closeable;
import java.util.List;

import com.crystalx.repository.exception.NoSuchObjectException;
import com.crystalx.repository.exception.RepositoryException;

public interface RepositoryController extends Closeable {

	String MODULE_SPLITTER = "/";
	
	ConnectionMode getMode();
	
	void createModule(String moduleName) throws RepositoryException;
	
	Document fetchDocument(String docuumentName, String revision) throws NoSuchObjectException;
	
	List<String> fetchDocumentRevisions(String docuumentName) throws NoSuchObjectException;
	
	String createNewDocument(String documentName, Document doc) throws RepositoryException;
	
	Document createDocumentBrench(String documentName, String baseRevision, Document newDocContent)
			throws RepositoryException, SecurityException;

}
