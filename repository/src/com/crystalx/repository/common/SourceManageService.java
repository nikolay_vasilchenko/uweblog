package com.crystalx.repository.common;

import java.io.OutputStream;
import java.util.List;

public interface SourceManageService {

	String calculateDifference(Source previous, Source current);
	
	void calculateDifference(Source previous, Source current, OutputStream out);

	String applyPatch(Source source, Source patch);
	
	void applyPatch(Source source, Source patch, OutputStream out);
	
	String applyPatches(Source origin, List<Source> patches);
	
	void applyPatches(Source origin, List<Source> patches, OutputStream out);

}

