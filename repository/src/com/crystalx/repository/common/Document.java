package com.crystalx.repository.common;

public interface Document extends Source {

	String revision();
	String repositoryPath();
}
