package com.crystalx.repository.common;

public class StringSource implements Source {

	private final String content;
	
	public StringSource(String content) {
		this.content = content;
	}
	
	@Override
	public String retreiveAllSource() {
		return content;
	}

}
